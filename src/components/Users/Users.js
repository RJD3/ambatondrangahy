import React, { useState, useEffect, useRef } from 'react';
import DataTable from '../Common/DataTables';
import { useDispatch, useSelector } from 'react-redux';
import { useToasts } from 'react-toast-notifications';
import {
  fetchUsers,
  clearUsers,
  addInvite,
  cleanInvites,
} from '../../store/actions/actions';
import { Button, Nav, NavItem, NavLink, Card } from 'reactstrap';
import ContentWrapper from '../Layout/ContentWrapper';
import { Link } from 'react-router-dom';
import './Users.css';
function Users() {
  const dispatch = useDispatch();

  const [users, setUsers] = useState({});
  const [usersDataTable, setUsersDataTable] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [activeTab, setActiveTab] = useState('joined');
  const [resetPage, setResetPage] = useState(false);

  const invites = useSelector(({ invites }) => invites);
  const state = useSelector((reduxState) => ({
    loading: reduxState.users.loading,
    usersData: reduxState.users.users,
    errorMessage: reduxState.users.error,
  }));
  const { loading, usersData, errorMessage } = state;
  const isMounted = useRef(false);
  const { addToast } = useToasts();
  useEffect(() => {
    dispatch(fetchUsers({ page: 1, limit: 10, type: activeTab }));
  }, [activeTab]);

  useEffect(() => {
    if (isMounted.current) {
      dispatch(fetchUsers({ page: currentPage, limit: 10, type: activeTab }));
    } else {
      isMounted.current = true;
    }
  }, [invites]);

  useEffect(() => {
    if (usersData && usersData.data) {
      const usersArray =
        usersData.data && usersData.data.users && usersData.data.users.docs
          ? usersData.data.users.docs
          : [];
      const dataT = usersArray.map((element) => {
        if (activeTab === 'joined') {
          return [
            element.username,
            element.firstName,
            element.lastName,
            element.email,
            element.role == 1
              ? 'Fan'
              : element.role == 3
              ? 'Investor'
              : 'Creator',
            element.phoneNumber,
          ];
        } else if (activeTab === 'invited') {
          return [element.guestUserId];
        } else if (activeTab === 'signedup') {
          return [
            element.firstName,
            element.lastName,
            element.email,
            element.phoneNumber,
            <div
              className='mr-1 mb-1'
              onClick={() => {
                dispatch(addInvite(element.phoneNumber, addToast));
              }}>
              <Button color='primary'>Invite</Button>
            </div>,
          ];
        }
      });

      setUsersDataTable(dataT);
      setUsers(usersData.data.users);
    } else {
      setUsersDataTable([]);
      setUsers({});
    }
  }, [usersData]);

  useEffect(() => {
    dispatch(clearUsers());
    if (errorMessage) {
      addToast(errorMessage, {
        appearance: 'error',
        autoDismiss: true,
      });
    }
  }, [errorMessage]);

  const columnsJoined = [
    'Username',
    'Name',
    'Last Name',
    'Email',
    'Role',
    'Phone Number',
  ];

  const columnsInvited = ['Phone Number'];

  const columnsSignedup = [
    'Name',
    'Last Name',
    'Email',
    'Phone Number',
    'Invite',
  ];
  const getPage = (page) => {
    if (page) {
      dispatch(fetchUsers({ page: page, limit: 10, type: activeTab }));
      setCurrentPage(page);
    }
  };

  return (
    <div style={{ width: '100%' }}>
      <ContentWrapper style={{ border: 'none' }}>
        <ContentWrapper
          style={{ border: 'none', marginBottom: -50, marginTop: -30 }}>
          <Nav tabs justified>
            <NavItem>
              <NavLink
                href='#joined'
                className={activeTab === 'joined' ? 'active' : ''}
                onClick={(e) => {
                  e.preventDefault();
                  setActiveTab('joined');
                  setResetPage(!resetPage);
                }}>
                Joined
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                href='#invited'
                className={activeTab === 'invited' ? 'active' : ''}
                onClick={(e) => {
                  e.preventDefault();
                  setActiveTab('invited');
                  setResetPage(!resetPage);
                }}>
                Invited
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                href='#signedup'
                className={activeTab === 'signedup' ? 'active' : ''}
                onClick={(e) => {
                  e.preventDefault();
                  setActiveTab('signedup');
                  setResetPage(!resetPage);
                }}>
                Invite
              </NavLink>
            </NavItem>
          </Nav>
        </ContentWrapper>
        <DataTable
          removeContentHeading
          loading={loading}
          columns={
            activeTab === 'signedup'
              ? columnsSignedup
              : activeTab === 'invited'
              ? columnsInvited
              : columnsJoined
          }
          getPage={getPage}
          data={usersDataTable}
          fulldata={users}
          reset={resetPage}
        />
      </ContentWrapper>
    </div>
  );
}

export default Users;
