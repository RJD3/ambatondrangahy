import React from 'react';
import 'loaders.css/loaders.css';
import 'spinkit/css/spinkit.css';

const BallSpinner = (props) => {
  return (
    <div className="card-body loader-demo d-flex align-items-center justify-content-center">
      <div className="ball-clip-rotate">
        <div></div>
      </div>
    </div>
  );
};

export default BallSpinner;
