/* eslint-disable no-nested-ternary */
/* eslint-disable default-case */
/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import MUIDataTable from 'mui-datatables';
import ContentWrapper from '../Layout/ContentWrapper';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Row, Col, Button } from 'reactstrap';
import Spinner from '../Common/Spinner';
import Modal from '../Common/AddInviteModal';

function DataTable({
  title = null,
  columns = null,
  data = [],
  fulldata = {},
  getPage = null,
  hasAdd = false,
  loading = false,
  textButton = null,
  link = null,
  modal,
  removeContentHeading = false,
  reset = null
}) {
  const [showModal, setShowModal] = useState(false);
  const [pagination, setPagination] = useState({
    page: 0,
    count: 1,
  });
  const [hasModal, setHasModal] = useState(false);
  const [term, setTerm] = useState(undefined);
  const [isEditable, setIsEditable] = useState(false);
  const history = useHistory();

  useEffect(() => {
    let timeout = null;
    if (term === undefined) {
      getPage(pagination.page + 1, term);
    } else {
      timeout = setTimeout(() => getPage(pagination.page + 1, term), 400);
    }
    return () => clearTimeout(timeout); 
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pagination.page, term]);

  useEffect(()=> {
    setPagination({page:0});
    getPage(1,term);
  },[reset])

  useEffect(() => {
    if (title === 'TITULLI') {
      setHasModal(true);
    } else if (hasAdd) {
      setIsEditable(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getMuiTheme = () =>
    createMuiTheme({
      palette: {
        primary: {
          main: '#1DA0D0',
        },
      },
      overrides: {
        MUIDataTableBodyCell: {
          root: { backgroundColor: 'transparent !important', color: 'inherit' },
        },
      },
    });

  const options = {
    filter: !hasModal,
    search: !hasModal,
    viewColumns: !hasModal,
    sort: false,
    filterType: 'dropdown',
    responsive: 'vertical',
    download: false,
    print: false,
    elevation: 1,
    serverSide: true,
    pagination: true,
    count: fulldata?.totalDocs || 0,
    rowsPerPage: 10,
    rowsPerPageOptions: [10],
    page: pagination.page,
    selectableRowsHideCheckboxes: true,
    selectableRowsOnClick: false,
    selectableRowsHeader: false,
    textLabels: {
      body: {
        noMatch: loading ? <Spinner /> : 'No matching',
      },
    },
    searchPlaceholder: 'Search Data',
    onSearchChange: (searchText) => {
      setTerm(searchText);
    },
    setRowProps: (row, dataIndex) => {
      if (dataIndex % 2 === 0) {
        return { style: { backgroundColor: '#FAFBFC' } };
      }
      return null;
    },
    onTableChange: (action, tableState) => {
      switch (action) {
        case 'changePage':
          setPagination({ page: tableState.page });
          break;
        case 'changeRowsPerPage':
          setPagination({ count: tableState.rowsPerPage });
          break;
      }
    },
  };

  return (
    <ContentWrapper style={{border:removeContentHeading?'none':'inherit'}} >
      
      {!removeContentHeading && <div className='d-flex justify-content-between content-heading'>
        <p>{title}</p>
        <div>
          {isEditable
            ? textButton && (
                <Button
                  onClick={() =>
                    link ? history.push(link) : setShowModal((prevState) => !prevState)
                  }
                  color='info'
                  className='btn-labeled'
                  style={{ display: 'flex', alignItems: 'center' }}>
                  <span className='btn-label'>
                    <i className='fas fa-plus' />
                  </span>
                  {textButton}
                </Button>
              )
            : null}
        </div>
      </div> }
      
      <Row>
        <Col xs={12} className='text-center'>
          <MuiThemeProvider theme={getMuiTheme()}>
            <MUIDataTable data={data} columns={columns} options={options} page={1} />
          </MuiThemeProvider>
        </Col>
      </Row>
      {modal && <Modal show={showModal} onHide={() => setShowModal(false)} />}
    </ContentWrapper>
  );
}

export default DataTable;
