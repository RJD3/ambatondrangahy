/* eslint-disable import/prefer-default-export */
import 'sweetalert2/src/sweetalert2.scss';
import Swal from 'sweetalert2';

const DialogBox = ({
  id,
  action,
  text,
  confirmText,
  cancelText,
  canceled,
  canceledDelete,
  title,
  data,
}) => {
  Swal.fire({
    title,
    text,
    showCancelButton: true,
    confirmButtonText: confirmText,
    confirmButtonColor: '#F05050',
    cancelButtonText: cancelText,
  }).then((result) => {
    if (result.isConfirmed) {
      if (!data) {
        action(id);
      } else {
        action(id, data);
      }
    } else if (result.dismiss === Swal.DismissReason.cancel) {
      Swal.fire(canceled, canceledDelete, 'error');
    }
  });
};

export { DialogBox };
