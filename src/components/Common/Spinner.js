/* eslint-disable react/prop-types */
import React from 'react';

const Spinner = ({ style }) => <div style={style} className="whirl standard" />;

export default Spinner;
