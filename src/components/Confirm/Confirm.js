import React, { useState, useEffect } from 'react';
import { Input } from 'reactstrap';
import { doLogin, cleanLoginError ,doConfirm,doResend,cleanLoginSuccess} from '../../store/actions/actions';
import { useDispatch, useSelector } from 'react-redux';
import { useForm,Controller } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useToasts } from 'react-toast-notifications';

function Confirm() {
  const dispatch = useDispatch()
  const history = useHistory();
  const [code,setCode] = useState()
  const token = useSelector(({ loginData }) => loginData.token);  
  const isLogged = useSelector(({loginData}) => loginData.loginSuccess);

  const { 
    handleSubmit,
    errors,
    control,
    reset
  } = useForm();
  const { addToast } = useToasts();
  const onSubmit = (data) => {
    const {code} = data
    dispatch(doConfirm(code,addToast));
  }
  const onResend = () => {
    dispatch(doResend(addToast))
  }

  useEffect(()=> {
    dispatch(cleanLoginSuccess());
    if(!isLogged){
      history.push('/login');
    }

    return () => dispatch(cleanLoginSuccess());
  },[])

  useEffect(() => {
    if (token) {
      history.push('/adminview');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token]);
  return (
    <div className='block-center mt-4 wd-xl col-3'>
      <div className='card card-flat'>
        <div className='card-body' style={{ padding: '2.9375rem' }}>
          <p className='text-center py-2' style={{ fontWeight: 'bold' }}>
            CONFIRMATION CODE
          </p>
          <p className='text-center'>
            Fill with the code that was sent to your phone!
          </p>
          <form onSubmit={handleSubmit(onSubmit)}>
          <div className='form-group'>
                <div className='input-group with-focus'>
                <Controller
                  name='code'
                  control={control}
                  rules={{
                    required: true,
                    
                  }}
                  defaultValue=''
                  render={({ onChange, value }) => (
                    <Input
                      onChange={onChange}
                      value={value}
                      type='code'
                      className='border-right-0'
                      placeholder='Confirmation Code'
                    />
                  )}
                />
                <div className='input-group-append'>
                  <span className='input-group-text text-muted bg-transparent border-left-0'>
                    <em className='fa fa-check'></em>
                  </span>
                </div>
                <span className='invalid-feedback'>Field is required</span>
                </div>
              </div>
            <button className='btn btn-block btn-primary mt-3' type='submit'>
              Confirm
            </button>
            <div style={{ textAlign: 'center', paddingTop: 30, fontSize: 15 }}>
              <Link onClick={()=> onResend()} >Resend code</Link>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Confirm;
