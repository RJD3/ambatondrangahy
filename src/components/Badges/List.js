import React, { useState, useEffect } from 'react';
import Swal from 'sweetalert2';
import moment from 'moment';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useToasts } from 'react-toast-notifications';
import { Button } from 'reactstrap';
import { Edit, DeleteOutline, Image } from '@material-ui/icons';

import ContentWrapper from '../Layout/ContentWrapper';
import MUIDataTable from 'mui-datatables';
import { getAllBadges, clearGetAllBadges, deleteOneBadge, clearDeleteOneBadge } from '../../store/actions/actions';

const getMUIColumns = () => ['Photo', 'Name', 'Description', 'Created At', 'Actions'];
const getMUIOptions = (onChangePage, onChangeRowsPerPage, onSearchChange, currentPage, currentLimit, count) => {
  return {
    selectableRows: 'none',
    print: false,
    filter: false,
    download: false,
    pagination: true,
    search: false,
    sort: false,
    serverSide: true,
    selectableRowsHeader: false,
    rowsPerPageOptions: [5, 10, 15],
    onChangePage: onChangePage,
    onChangeRowsPerPage: onChangeRowsPerPage,
    onSearchChange: onSearchChange,
    page: currentPage,
    rowsPerPage: currentLimit,
    count: count,
  };
};
const getMUIData = (badges, dispatch, addToast, history) => {
  if (!badges || (badges && !badges.length)) return [];

  return badges.map((badge) => {
    const { _id, name, description, photo, createdAt } = badge;
    return [
      photo && photo.location ? (
        <img src={photo.location} alt="Badge Photo" style={{ width: 100, height: 100, objectFit: 'cover' }} />
      ) : (
        <div style={{ width: 100, height: 100 }}>
          <Image style={{ width: 100, height: 100 }} />
        </div>
      ),
      name,
      description,
      moment(createdAt).format('YYYY/MM/DD'),
      <div>
        <Button
          color="primary"
          style={{ margin: '2.5px' }}
          onClick={() => {
            history.push(`/badges/edit/${_id}`);
          }}
        >
          <Edit style={{ fontSize: '20px' }} />
        </Button>
        <Button
          color="danger"
          style={{ margin: '2.5px' }}
          onClick={() => {
            Swal.fire({
              title: 'Are you sure you want to delete this badge?',
              text: 'Deleting this badge will also delete it from all of the users and gangs it belongs to!',
              padding: '50px',
              showCancelButton: true,
              confirmButtonText: 'Yes, delete it!',
              confirmButtonColor: '#F05050',
              cancelButtonText: 'No, cancel it!',
            }).then((result) => {
              if (result.isConfirmed) dispatch(deleteOneBadge({ badgeId: _id }, { addToast }));
            });
          }}
        >
          <DeleteOutline style={{ fontSize: '20px' }} />
        </Button>
      </div>,
    ];
  });
};

const defaultPage = 0;
const defaultLimit = 10;

const List = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { addToast } = useToasts();

  const getAllResponse = useSelector(({ badges }) => badges.getAll);
  const deleteOneResponse = useSelector(({ badges }) => badges.deleteOne);

  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(defaultPage);
  const [limit, setLimit] = useState(defaultLimit);
  const [localBadges, setLocalBadges] = useState([]);
  const [badgesCount, setBadgesCount] = useState(0);

  useEffect(() => {
    const payload = { page: page + 1, limit: limit, pagination: true };
    dispatch(getAllBadges(payload));
  }, [page, limit]);

  useEffect(() => {
    if (deleteOneResponse && deleteOneResponse.success) {
      const payload = { page: defaultPage + 1, limit: defaultLimit, pagination: true };
      dispatch(getAllBadges(payload));

      setPage(defaultPage);
      setLimit(defaultLimit);
    }
  }, [deleteOneResponse]);

  useEffect(() => {
    if (getAllResponse) {
      switch (true) {
        case getAllResponse.loading:
          setLoading(true);
          break;
        case getAllResponse.success:
          const { docs, totalDocs } = getAllResponse.data.badges;

          const muiBadges = getMUIData(docs, dispatch, addToast, history);
          setLocalBadges(muiBadges);
          setBadgesCount(totalDocs);

          setLoading(false);
          break;
        case getAllResponse.error:
          setLoading(false);
          break;
      }
    }
  }, [getAllResponse]);

  useEffect(() => {
    return () => {
      dispatch(clearGetAllBadges());
      dispatch(clearDeleteOneBadge());
    };
  }, []);

  const onChangePage = (page) => {
    setPage(page);
  };

  const onChangeRowsPerPage = (limit) => {
    setPage(defaultPage);
    setLimit(limit);
  };

  return (
    <ContentWrapper>
      <div className="d-flex justify-content-between content-heading">
        <p>Badges</p>
        <div>
          <Button
            onClick={() => history.push('/badges/create')}
            color="info"
            className="btn-labeled"
            style={{ display: 'flex', alignItems: 'center' }}
          >
            <span className="btn-label">
              <i className="fas fa-plus" />
            </span>
            Create
          </Button>
        </div>
      </div>
      <MUIDataTable
        data={localBadges}
        columns={getMUIColumns()}
        options={getMUIOptions(onChangePage, onChangeRowsPerPage, null, page, limit, badgesCount)}
      />
    </ContentWrapper>
  );
};

export default List;
