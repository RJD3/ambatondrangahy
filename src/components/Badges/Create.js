import React, { useState, useEffect, useMemo } from 'react';
import ApiService from '../../services/ApiService';
import moment from 'moment';
import { v4 } from 'uuid';
import { useToasts } from 'react-toast-notifications';
import { useDropzone } from 'react-dropzone';
import { useHistory } from 'react-router-dom';
import { Chip, Badge, Avatar } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { ArrowBack, NextWeek, Close } from '@material-ui/icons';
import { Button, Row, Col, Card, CardHeader, CardBody, FormGroup, Input } from 'reactstrap';

import ContentWrapper from '../Layout/ContentWrapper';
import { createBadge, clearCreateBadge } from '../../store/actions/actions';

const baseStyle = {
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '20px',
  borderWidth: 2,
  borderRadius: 2,
  borderColor: '#929292',
  borderStyle: 'dashed',
  backgroundColor: '#fafafa',
  color: '#929292',
  outline: 'none',
  transition: 'border .24s ease-in-out',
  cursor: 'pointer',
};
const thumbsContainer = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  marginTop: 16,
};
const dropzoneOptions = {
  multiple: false,
  maxFiles: 1,
  accept: {
    'image/*': [],
  },
};

const Create = (props) => {
  const [loading, setLoading] = useState(false);
  const [file, setFile] = useState(null);

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');

  const createdResult = useSelector(({ badges }) => badges.create);

  const history = useHistory();
  const dispatch = useDispatch();
  const style = useMemo(() => ({ ...baseStyle }), []);
  const { getRootProps, getInputProps } = useDropzone({ ...dropzoneOptions, onDrop: (files) => addFile(files) });
  const { addToast } = useToasts();

  const addFile = (acceptedFiles) => {
    const tempFile = Object.assign(acceptedFiles[0], { id: v4(), preview: URL.createObjectURL(acceptedFiles[0]) });
    setFile(tempFile);
  };

  const removeFile = () => {
    setFile(null);
  };

  const addBadge = (e) => {
    e.preventDefault();

    if (!name || !description) {
      addToast('Please fill all fields to create a new badge!', { appearance: 'error', autoDismiss: true });
      return;
    }

    if (!file) {
      addToast('Please upload 1 badge!', { appearance: 'error', autoDismiss: true });
      return;
    }

    const payload = {
      name,
      description,
      file,
    };
    dispatch(createBadge(payload, { addToast, history }));
  };

  const handleNameChange = (e) => setName(e.target.value);
  const handleDescriptionChange = (e) => setDescription(e.target.value);

  useEffect(() => {
    if (createdResult) {
      switch (true) {
        case createdResult.loading:
          setLoading(true);
          break;
        case createdResult.success || createdResult.error:
          setLoading(false);
          break;
        default:
          break;
      }
    }
  }, [createdResult]);

  useEffect(() => {
    // Make sure to revoke the data uris to avoid memory leaks, will run on unmount
    return () => {
      dispatch(clearCreateBadge());
    };
  }, []);

  return (
    <ContentWrapper>
      <div className="d-flex justify-content-between content-heading">
        <p>Badges</p>
        <div>
          <Button
            onClick={() => history.goBack()}
            color="info"
            className="btn-labeled"
            style={{ display: 'flex', alignItems: 'center' }}
          >
            <span className="btn-label">
              {/* <ArrowBack style={{ fontSize: '17.5px' }} /> */}
              <i className="fas fa-arrow-left" />
            </span>
            Back
          </Button>
        </div>
      </div>

      <Row>
        <Col xs={12} md={6} xl={6}>
          <Card className="card-default" style={{ padding: '15px' }}>
            <section className="container">
              <div {...getRootProps({ style })}>
                <input {...getInputProps()} />
                <p>Drag and drop one badge here, or click to select badge...</p>
                <p>Only 1 badge allowed</p>
              </div>
              <aside style={thumbsContainer}>
                {file && (
                  <Badge
                    key={file.id}
                    overlap="circle"
                    style={{ height: 100, width: 100, margin: '5px' }}
                    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                    badgeContent={
                      <Close
                        variant="contained"
                        onClick={() => {
                          removeFile();
                        }}
                        style={{
                          cursor: 'pointer',
                          fontSize: 30,
                          padding: '5px',
                          backgroundColor: '#ffffff',
                          boxShadow: '0px 0px 10px rgba(0,0,0,0.6)',
                          borderRadius: '50%',
                          marginBottom: 7,
                          marginLeft: 7,
                          color: 'black',
                        }}
                      />
                    }
                  >
                    <img
                      src={file.preview}
                      style={{ width: '100%', height: '100%', objectFit: 'contain' }}
                      // Revoke data uri after image is loaded
                      onLoad={() => {
                        URL.revokeObjectURL(file.preview);
                      }}
                    />
                  </Badge>
                )}
              </aside>
            </section>
          </Card>
        </Col>
        <Col xs={12} md={6} xl={6}>
          <Card className="card-default">
            <CardBody>
              <form>
                <FormGroup>
                  <label>Name</label>
                  <Input
                    type="text"
                    placeholder="Name..."
                    disabled={loading}
                    value={name}
                    onChange={(e) => handleNameChange(e)}
                  />
                </FormGroup>
                <FormGroup>
                  <label>Description</label>
                  <Input
                    type="text"
                    placeholder="Description..."
                    disabled={loading}
                    value={description}
                    onChange={(e) => handleDescriptionChange(e)}
                  />
                </FormGroup>
                <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                  <button
                    className="btn btn-md btn-primary"
                    type="submit"
                    disabled={loading}
                    onClick={(e) => addBadge(e)}
                  >
                    Submit
                  </button>
                </div>
              </form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </ContentWrapper>
  );
};

export default Create;
