import React, { useState, useEffect, useMemo } from 'react';
import moment from 'moment';
import Swal from 'sweetalert2';
import Skeleton from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';
import MUIDataTable from 'mui-datatables';
import { v4 } from 'uuid';
import { useToasts } from 'react-toast-notifications';
import { useDropzone } from 'react-dropzone';
import { useHistory } from 'react-router-dom';
import { Chip, Badge, Avatar } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { ArrowBack, NextWeek, Close, DeleteOutline, Check } from '@material-ui/icons';
import { Button, Row, Col, Card, CardHeader, CardBody, FormGroup, Input } from 'reactstrap';

import ContentWrapper from '../Layout/ContentWrapper';
import {
  updateOneBadge,
  clearUpdateOneBadge,
  getOneBadge,
  clearGetOneBadge,
  getBadgeUsers,
  clearGetBadgeUsers,
  getBadgeCandidates,
  clearGetBadgeCandidates,
  assignBadge,
  clearAssignBadge,
  removeBadge,
  clearRemoveBadge,
} from '../../store/actions/actions';

const baseStyle = {
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '20px',
  borderWidth: 2,
  borderRadius: 2,
  borderColor: '#929292',
  borderStyle: 'dashed',
  backgroundColor: '#fafafa',
  color: '#929292',
  outline: 'none',
  transition: 'border .24s ease-in-out',
  cursor: 'pointer',
};
const thumbsContainer = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  marginTop: 16,
};
const dropzoneOptions = {
  multiple: true,
  maxFiles: 5,
  accept: {
    'image/*': [],
  },
};

const getMUIData = (users, dispatch, addToast, history, type, badgeId) => {
  if (!users || (users && !users.length)) return [];
  return users.map((user) => {
    const { _id, username, firstName, lastName, email, phoneNumber } = user;
    return [
      username || 'NA',
      firstName || 'NA',
      lastName || 'NA',
      email || 'NA',
      phoneNumber || 'NA',
      <div>
        <Button
          color={type === usersType ? 'danger' : 'success'}
          style={{ margin: '2.5px' }}
          onClick={() => {
            Swal.fire({
              title: 'Are you sure?',
              text:
                type === usersType
                  ? 'Are you sure you want to remove user from this badge?'
                  : 'Are you sure you want to add this user to this badge?',
              padding: '50px',
              showCancelButton: true,
              confirmButtonText: type === usersType ? 'Yes, remove from badge!' : 'Yes, add to badge!',
              confirmButtonColor: type === usersType ? '#F05050' : '#21A240',
              cancelButtonText: 'No, cancel!',
            }).then((result) => {
              if (result.isConfirmed) {
                switch (type) {
                  case usersType:
                    dispatch(removeBadge({ badgeId, userId: _id }, { addToast }));
                    break;
                  case candidatesType:
                    dispatch(assignBadge({ badgeId, userId: _id }, { addToast }));
                    break;
                  default:
                    break;
                }
              }
            });
          }}
        >
          {type === usersType ? <DeleteOutline style={{ fontSize: '20px' }} /> : <Check style={{ fontSize: '20px' }} />}
        </Button>
      </div>,
    ];
  });
};
const getMUIColumns = () => ['Username', 'First Name', 'Last Name', 'Email', 'Phone Number', 'Actions'];
const getMUIOptions = (onChangePage, onChangeRowsPerPage, onSearchChange, type, currentPage, currentLimit, count) => {
  return {
    selectableRows: 'none',
    print: false,
    filter: false,
    download: false,
    pagination: true,
    search: true,
    sort: false,
    serverSide: true,
    selectableRowsHeader: false,
    rowsPerPageOptions: [5, 10, 15],
    onChangePage: (limit) => onChangePage(limit, type),
    onChangeRowsPerPage: (limit) => onChangeRowsPerPage(limit, type),
    onSearchChange: (search) => onSearchChange(search, type),
    page: currentPage,
    rowsPerPage: currentLimit,
    count: count,
  };
};

const defaultPage = 0;
const defaultLimit = 10;

const usersType = 'users';
const candidatesType = 'candidates';

const Edit = (props) => {
  const badgeId = props.match.params.id;
  const [loading, setLoading] = useState(false);
  const [file, setFile] = useState(null);
  const [previousFile, setPreviousFile] = useState(null);

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');

  const [usersPage, setUsersPage] = useState(defaultPage);
  const [usersLimit, setUsersLimit] = useState(defaultLimit);
  const [localUsers, setLocalUsers] = useState([]);
  const [usersCount, setUsersCount] = useState(0);
  const [usersSearch, setUsersSearch] = useState('');

  const [candidatesPage, setCandidatesPage] = useState(defaultPage);
  const [candidatesLimit, setCandidatesLimit] = useState(defaultLimit);
  const [localCandidates, setLocalCandidates] = useState([]);
  const [candidatesCount, setCandidatesCount] = useState(0);
  const [candidatesSearch, setCandidatesSearch] = useState('');

  const getOneResult = useSelector(({ badges }) => badges.getOne);
  const updateOneResult = useSelector(({ badges }) => badges.updateOne);
  const getBadgeUsersResult = useSelector(({ badges }) => badges.getBadgeUsers);
  const getBadgeCandidatesResult = useSelector(({ badges }) => badges.getBadgeCandidates);
  const assignBadgeResult = useSelector(({ badges }) => badges.assignBadge);
  const removeBadgeResult = useSelector(({ badges }) => badges.removeBadge);

  const history = useHistory();
  const dispatch = useDispatch();
  const style = useMemo(() => ({ ...baseStyle }), []);
  const { getRootProps, getInputProps } = useDropzone({ ...dropzoneOptions, onDrop: (files) => addFile(files) });
  const { addToast } = useToasts();

  const addFile = (acceptedFiles) => {
    setPreviousFile(null);
    const tempFile = Object.assign(acceptedFiles[0], { id: v4(), preview: URL.createObjectURL(acceptedFiles[0]) });
    setFile(tempFile);
  };

  const removeFile = () => {
    setPreviousFile(null);
    setFile(null);
  };

  const editBadge = (e) => {
    e.preventDefault();

    if (!name || !description) {
      addToast('Please fill all fields to create a new code!', { appearance: 'error', autoDismiss: true });
      return;
    }

    if (!file && !previousFile) {
      addToast('Please upload at least 1 badge!', { appearance: 'error', autoDismiss: true });
      return;
    }

    const payload = {
      badgeId,
      name,
      description,
      file,
    };
    dispatch(updateOneBadge(payload, { addToast, history }));
  };

  const handleNameChange = (e) => setName(e.target.value);
  const handleDescriptionChange = (e) => setDescription(e.target.value);

  const onChangePage = (page, type) => {
    switch (type) {
      case usersType:
        setUsersPage(page);
        break;
      case candidatesType:
        setCandidatesPage(page);
        break;
      default:
        break;
    }
  };

  const onChangeRowsPerPage = (limit, type) => {
    switch (type) {
      case usersType:
        setUsersPage(defaultPage);
        setUsersLimit(limit);
        break;
      case candidatesType:
        setCandidatesPage(defaultPage);
        setCandidatesLimit(limit);
        break;
      default:
        break;
    }
  };

  const onSearchChange = (search, type) => {
    switch (type) {
      case usersType:
        const usersPayload = { badgeId, page: defaultPage + 1, limit: defaultLimit, pagination: true, search };
        dispatch(getBadgeUsers(usersPayload));
        setUsersSearch(search);
        break;
      case candidatesType:
        const candidatesPayload = { badgeId, page: defaultPage + 1, limit: defaultLimit, pagination: true, search };
        dispatch(getBadgeCandidates(candidatesPayload));
        setCandidatesSearch(search);
        break;
      default:
        break;
    }
  };

  const recallUserData = () => {
    const payload = { badgeId, page: defaultPage + 1, limit: defaultLimit, pagination: true };

    dispatch(getBadgeUsers(payload));
    dispatch(getBadgeCandidates(payload));

    setUsersPage(defaultPage);
    setUsersLimit(defaultLimit);

    setCandidatesPage(defaultPage);
    setUsersLimit(defaultLimit);
  };

  useEffect(() => {
    dispatch(getOneBadge({ badgeId }));

    const payload = { badgeId, page: defaultPage + 1, limit: defaultLimit, pagination: true };
    dispatch(getBadgeUsers(payload));
    dispatch(getBadgeCandidates(payload));
  }, []);

  useEffect(() => {
    if (getOneResult) {
      switch (true) {
        case getOneResult.loading:
          setLoading(true);
          break;
        case getOneResult.success:
          setLoading(false);
          const { badge } = getOneResult.data;
          const { name, description, photo } = badge;

          setName(name);
          setDescription(description);
          setPreviousFile(photo);
          break;
        case getOneResult.error:
          setLoading(false);
          const errorMessage = getOneResult.errorMessage || 'Failed to get this badge!';
          addToast(errorMessage, { appearance: 'error', autoDismiss: true });
          break;
        default:
          break;
      }
    }
  }, [getOneResult]);

  useEffect(() => {
    if (updateOneResult) {
      switch (true) {
        case updateOneResult.loading:
          setLoading(true);
          break;
        case updateOneResult.succes || updateOneResult.error:
          setLoading(false);
          break;
        default:
          break;
      }
    }
  }, [updateOneResult]);

  useEffect(() => {
    if (getBadgeUsersResult) {
      switch (true) {
        case getBadgeUsersResult.loading:
          if (!usersSearch) setLoading(true);
          break;
        case getBadgeUsersResult.success:
          const { docs, totalDocs } = getBadgeUsersResult.data.users;

          const muiUsers = getMUIData(docs, dispatch, addToast, history, usersType, badgeId);
          setLocalUsers(muiUsers);
          setUsersCount(totalDocs);

          setLoading(false);
          break;
        case getBadgeUsersResult.error:
          setLoading(false);
          break;
      }
    }
  }, [getBadgeUsersResult]);

  useEffect(() => {
    if (getBadgeCandidatesResult) {
      switch (true) {
        case getBadgeCandidatesResult.loading:
          if (!candidatesSearch) setLoading(true);
          break;
        case getBadgeCandidatesResult.success:
          const { docs, totalDocs } = getBadgeCandidatesResult.data.users;

          const muiUsers = getMUIData(docs, dispatch, addToast, history, candidatesType, badgeId);
          setLocalCandidates(muiUsers);
          setCandidatesCount(totalDocs);

          setLoading(false);
          break;
        case getBadgeCandidatesResult.error:
          setLoading(false);
          break;
      }
    }
  }, [getBadgeCandidatesResult]);

  useEffect(() => {
    if (assignBadgeResult)
      switch (true) {
        case assignBadgeResult.loading:
          setLoading(true);
          break;
        case assignBadgeResult.success:
          setLoading(false);
          recallUserData();
          break;
        case assignBadgeResult.error:
          setLoading(false);
          break;
        default:
          break;
      }
  }, [assignBadgeResult]);

  useEffect(() => {
    if (removeBadgeResult)
      switch (true) {
        case removeBadgeResult.loading:
          setLoading(true);
          break;
        case removeBadgeResult.success:
          setLoading(false);
          recallUserData();
          break;
        case removeBadgeResult.error:
          setLoading(false);
          break;
        default:
          break;
      }
  }, [removeBadgeResult]);

  useEffect(() => {
    const payload = { badgeId, page: usersPage + 1, limit: usersLimit, pagination: true };
    dispatch(getBadgeUsers(payload));
  }, [usersPage, usersLimit]);

  useEffect(() => {
    const payload = { badgeId, page: candidatesPage + 1, limit: candidatesLimit, pagination: true };
    dispatch(getBadgeCandidates(payload));
  }, [candidatesPage, candidatesLimit]);

  useEffect(() => {
    // Make sure to revoke the data uris to avoid memory leaks, will run on unmount
    return () => {
      dispatch(clearGetBadgeUsers());
      dispatch(clearGetBadgeCandidates());
      dispatch(clearAssignBadge());
      dispatch(clearRemoveBadge());
      dispatch(clearUpdateOneBadge());
    };
  }, []);

  return (
    <ContentWrapper>
      <div className="d-flex justify-content-between content-heading">
        <p>Badges</p>
        <div>
          <Button
            onClick={() => history.goBack()}
            color="info"
            className="btn-labeled"
            style={{ display: 'flex', alignItems: 'center' }}
          >
            <span className="btn-label">
              {/* <ArrowBack style={{ fontSize: '17.5px' }} /> */}
              <i className="fas fa-arrow-left" />
            </span>
            Back
          </Button>
        </div>
      </div>

      <Row>
        <Col xs={12} md={6} xl={6}>
          <Card className="card-default" style={{ padding: '15px' }}>
            <section className="container">
              <div {...getRootProps({ style })}>
                <input {...getInputProps()} />
                <p>Drag and drop one badge here, or click to select badge...</p>
                <p>Only 1 badge allowed</p>
              </div>
              <aside style={thumbsContainer}>
                {file ? (
                  <Badge
                    key={file.name}
                    overlap="circle"
                    style={{ height: 100, width: 100, margin: '5px' }}
                    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                    badgeContent={
                      <Close
                        variant="contained"
                        onClick={() => {
                          removeFile();
                        }}
                        style={{
                          cursor: 'pointer',
                          fontSize: 30,
                          padding: '5px',
                          backgroundColor: '#ffffff',
                          boxShadow: '0px 0px 10px rgba(0,0,0,0.6)',
                          borderRadius: '50%',
                          marginBottom: 7,
                          marginLeft: 7,
                          color: 'black',
                        }}
                      />
                    }
                  >
                    <img
                      src={file.preview}
                      style={{ width: '100%', height: '100%', objectFit: 'contain' }}
                      // Revoke data uri after image is loaded
                      onLoad={() => {
                        URL.revokeObjectURL(file.preview);
                      }}
                    />
                  </Badge>
                ) : previousFile ? (
                  <Badge
                    key={previousFile && previousFile.location}
                    overlap="circle"
                    style={{ height: 100, width: 100, margin: '5px' }}
                    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                    badgeContent={
                      <Close
                        variant="contained"
                        onClick={() => {
                          removeFile();
                        }}
                        style={{
                          cursor: 'pointer',
                          fontSize: 30,
                          padding: '5px',
                          backgroundColor: '#ffffff',
                          boxShadow: '0px 0px 10px rgba(0,0,0,0.6)',
                          borderRadius: '50%',
                          marginBottom: 7,
                          marginLeft: 7,
                          color: 'black',
                        }}
                      />
                    }
                  >
                    <img
                      src={previousFile && previousFile.location}
                      style={{ width: '100%', height: '100%', objectFit: 'contain' }}
                    />
                  </Badge>
                ) : null}
              </aside>
            </section>
          </Card>
        </Col>
        <Col xs={12} md={6} xl={6}>
          <Card className="card-default">
            <CardBody>
              <form>
                <FormGroup>
                  <label>Name</label>
                  <Input
                    type="text"
                    placeholder="Name..."
                    disabled={loading}
                    value={name}
                    onChange={(e) => handleNameChange(e)}
                  />
                </FormGroup>
                <FormGroup>
                  <label>Description</label>
                  <Input
                    type="text"
                    placeholder="Contact Name..."
                    disabled={loading}
                    value={description}
                    onChange={(e) => handleDescriptionChange(e)}
                  />
                </FormGroup>
                <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                  <button
                    className="btn btn-md btn-primary"
                    type="submit"
                    disabled={loading}
                    onClick={(e) => editBadge(e)}
                  >
                    Submit
                  </button>
                </div>
              </form>
            </CardBody>
          </Card>
        </Col>
      </Row>
      <Row style={{ marginTop: '10px' }}>
        <Col xs={12} md={12} xl={12}>
          <MUIDataTable
            title="Users with this badge"
            data={localUsers}
            columns={getMUIColumns()}
            options={getMUIOptions(
              onChangePage,
              onChangeRowsPerPage,
              onSearchChange,
              usersType,
              usersPage,
              usersLimit,
              usersCount
            )}
          />
        </Col>
      </Row>
      <Row style={{ marginTop: '20px' }}>
        <Col xs={12} md={12} xl={12}>
          <MUIDataTable
            title="Users without this badge"
            data={loading ? [] : localCandidates}
            columns={getMUIColumns()}
            options={getMUIOptions(
              onChangePage,
              onChangeRowsPerPage,
              onSearchChange,
              candidatesType,
              candidatesPage,
              candidatesLimit,
              candidatesCount
            )}
          />
        </Col>
      </Row>
    </ContentWrapper>
  );
};

export default Edit;
