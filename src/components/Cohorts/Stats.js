/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react';
import DataTable from '../Common/DataTables';
import {
  fetchCohortUsers,
  cleanCohortUsers,
  fetchCohortUsersCurrent,
} from '../../store/actions/actions';
import { useSelector, useDispatch } from 'react-redux';
import { useToasts } from 'react-toast-notifications';
import { Row, Col } from 'reactstrap';

import 'react-datetime/css/react-datetime.css';

function Stats(props) {
  const [cohortUsersDataTable, setCohortUsersDataTable] = useState([]);
  const [cohortUsers, setCohortUsers] = useState({});
  const [showStatsData, setStatsData] = useState([]);

  const state = useSelector((reduxState) => ({
    loading: reduxState.stats.loading,
    cohortUsersData: reduxState.stats.users,
    errorMessage: reduxState.stats.error,
    cohortsData: reduxState.cohorts.cohorts,
  }));

  const cohortId = props.match.params.id;

  const { loading, cohortUsersData, errorMessage, cohortsData } = state;
  const dispatch = useDispatch();
  const { addToast } = useToasts();

  useEffect(() => {
    if (cohortUsersData && cohortUsersData.data) {
      let currentPoints = '';
      const dataT = cohortUsersData.data.users.docs.map((element) => {
        if (element.currentPoints == null) {
          currentPoints = element.pointsHistory.points;
        } else if (element.currentPoints !== null) {
          currentPoints = element.currentPoints.points;
        }
        return [
          element.username,
          element.startupName,
          currentPoints,
          element.phoneNumber,
          element.city?.name || 'Planet Earth',
        ];
      });

      setCohortUsersDataTable(dataT);
      setCohortUsers(cohortUsersData.data.users);
    }
  }, [cohortUsersData]);

  useEffect(() => {
    if (cohortUsersData && cohortUsersData.data) {
      setStatsData(cohortUsersData.data.cohort);
    }
  }, [cohortUsersData]);

  const getPage = (page) => {
    if (page) {
      if (cohortsData && cohortsData.data) {
        cohortsData.data.cohorts.docs.forEach((element) => {
          if (element._id === props.match.params.id) {
            if (element.isCurrent === true) {
              dispatch(fetchCohortUsersCurrent(page));
            } else if (element.isCurrent !== true) {
              dispatch(fetchCohortUsers(cohortId, page));
            }
          }
        });
      }
    }
  };

  useEffect(() => {
    dispatch(cleanCohortUsers());
    if (errorMessage) {
      addToast(errorMessage, {
        appearance: 'error',
        autoDismiss: true,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [errorMessage]);

  const columns = ['Name', 'Startup Name', 'Points', 'Phone Number', 'City'];
  return (
    <div style={{ paddingTop: '10px' }}>
      <Row>
        <Col lg='5'>
          <div
            className='card-header'
            style={{
              marginTop: '20px',
              height: '77px',
              backgroundColor: 'white',
              borderRadius: '0px',
              fontSize: '1.5rem',
              lineHeight: '1.1',
              color: '#929292',
              fontWeight: 'normal',
              alignItems: 'center',
              display: 'flex',
            }}>
            Cohort
          </div>
          <div
            className='card'
            style={{
              marginTop: '20px',
              marginRight: '20px',
              marginLeft: '20px',
            }}>
            {showStatsData == null && showStatsData === undefined ? null : (
              <div>
                <h4
                  style={{
                    marginLeft: '5px',
                    marginBottom: '10px',
                    marginTop: '10px',
                  }}>
                  Cohort details
                </h4>
                <table style={{ tableLayout: 'fixed' }} className='table'>
                  <tbody>
                    <tr>
                      <td>
                        <strong>Cohort title:</strong>
                      </td>
                      <td>{showStatsData.title}</td>
                    </tr>
                    <tr>
                      <td>
                        <strong>Start date:</strong>
                      </td>
                      <td>{showStatsData.startDate}</td>
                    </tr>
                  </tbody>
                </table>
                {showStatsData.photo == null &&
                showStatsData.photo === undefined ? null : (
                  <img
                    className='img-fluid img-thumbnail full-width'
                    style={{
                      height: '538px',
                      objectFit: 'contain',
                      borderRadius: '0px',
                      backgroundColor: 'white',
                    }}
                    src={
                      showStatsData &&
                      showStatsData.photo &&
                      showStatsData.photo.location
                        ? showStatsData.photo.location
                        : null
                    }
                    alt='Contact'
                  />
                )}
              </div>
            )}
            <div className='card-body'></div>
          </div>
        </Col>
        <Col lg='7'>
          <DataTable
            columns={columns}
            loading={loading}
            getPage={getPage}
            data={cohortUsersDataTable}
            fulldata={cohortUsers}
            title='Stats'
          />
        </Col>
      </Row>
    </div>
  );
}

export default Stats;
