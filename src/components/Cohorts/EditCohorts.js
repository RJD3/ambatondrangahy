import React, { useEffect, useState, useMemo } from 'react';
import moment from 'moment';
import Datetime from 'react-datetime';
import { useToasts } from 'react-toast-notifications';
import { DialogBox } from '../Common/DialogBox';
import { useHistory } from 'react-router-dom';
import { useDropzone } from 'react-dropzone';
import { useForm, Controller } from 'react-hook-form';
import { useSelector, useDispatch } from 'react-redux';
import { Input, Button, Card, CardBody, FormGroup, Row, Col } from 'reactstrap';
import {
  addCohort,
  cleanCohorts,
  fetchCohort,
  updateCohort,
  deleteCohort,
  uploadFile,
} from '../../store/actions/actions';

import Spinner from '../Common/Spinner';
import ContentWrapper from '../Layout/ContentWrapper';

import 'react-datetime/css/react-datetime.css';

const baseStyle = {
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '20px',
  borderWidth: 2,
  borderRadius: 2,
  borderColor: '#929292',
  borderStyle: 'dashed',
  backgroundColor: '#fafafa',
  color: '#929292',
  outline: 'none',
  transition: 'border .24s ease-in-out',
  cursor: 'pointer',
};
const dropzoneOptions = {
  multiple: false,
  accept: {
    'image/*': [],
  },
};

function EditAdminView(props) {
  const [file, setFile] = useState(null);
  const [cohortsState, setCohortsState] = useState([]);
  const [previewImg, setpreviewedImg] = useState(null);

  const { register, handleSubmit, setValue, errors, control, reset } = useForm();
  const cohortId = props.match.params.id;

  const state = useSelector(({ cohorts }) => ({
    addedCohort: cohorts.addedCohort,
    loading: cohorts.loading,
    cohortsData: cohorts.cohort,
    errorMessage: cohorts.error,
    updatedCohort: cohorts.updatedCohort,
    deletedCohort: cohorts.deletedCohort,
  }));

  const { cohortsData, loading, errorMessage, updatedCohort, deletedCohort, addedCohort } = state;

  const { addToast } = useToasts();
  const history = useHistory();
  const dispatch = useDispatch();

  const style = useMemo(() => ({ ...baseStyle }), []);
  const { getRootProps, getInputProps } = useDropzone({ ...dropzoneOptions, onDrop: (files) => onDrop(files) });
  const onDrop = (acceptedFile) => {
    const tempFile = acceptedFile[0];
    const fileType = tempFile.type.split('/').pop();
    const validFileTypes = ['jpg', 'jpe', 'png', 'jpeg'];
    const maxFileSize = 20 * 1024 * 1024;

    if (!tempFile || !fileType) {
      addToast('File missing!', {
        appearance: 'error',
        autoDismiss: 'true',
      });
      return;
    }

    if (!validFileTypes.includes(fileType)) {
      addToast(`The submitted file format is not supported! Supported formats are: ${validFileTypes.toString()}!`, {
        appearance: 'error',
        autoDismiss: 'true',
      });
      return;
    }

    if (maxFileSize < tempFile.size) {
      addToast(`The file size limit is 20MB!`, {
        appearance: 'error',
        autoDismiss: 'true',
      });
      return;
    }

    const reader = new window.FileReader();
    reader.readAsDataURL(tempFile);
    reader.onload = function () {
      const imageDataUrl = reader.result;
      setpreviewedImg(imageDataUrl);
    };

    setFile(tempFile);
  };

  useEffect(() => {
    register({ name: 'title' }, { required: true });
    register({ name: 'startDate' }, { required: true });
    register({ name: 'endDate' }, { required: true });
    register({ name: 'description' }, { required: true });
    register({ name: 'textButton' }, { required: true });
    register({ name: 'linkButton' }, { required: true });
  }, [register]);

  useEffect(() => {
    if (props.match.params.id && cohortsData && cohortsData.success) {
      const { cohort } = cohortsData.data;
      setCohortsState(cohortsData.data.cohort.photo);
      setValue('title', cohort.title);
      setValue('startDate', moment(cohort.startDate));
      setValue('endDate', moment(cohort.endDate));
      setValue('description', cohort.description);
      setValue('textButton', cohort.textButton);
      setValue('linkButton', cohort.linkButton);
      setValue('fileURL', cohort.fileURL);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cohortsData]);

  useEffect(() => {
    if (cohortId) {
      dispatch(fetchCohort(cohortId));
    }
    return () => {
      dispatch(cleanCohorts());
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (addedCohort && addedCohort.success) {
      addToast('Cohort is added successfully', {
        appearance: 'success',
        autoDismiss: true,
      });
      history.goBack();
    }
    reset();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [addedCohort]);

  useEffect(() => {
    if (deletedCohort && deletedCohort.success) {
      addToast('Cohort deleted successfully', {
        appearance: 'success',
        autoDismiss: true,
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
      history.goBack();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [deletedCohort]);

  useEffect(() => {
    if (updatedCohort && updatedCohort.success) {
      addToast('Cohort updated successfully', {
        appearance: 'success',
        autoDismiss: true,
      });
      history.goBack();
    }
    reset();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [updatedCohort]);

  useEffect(() => {
    if (errorMessage) {
      addToast(errorMessage, {
        appearance: 'error',
        autoDismiss: true,
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
      dispatch(cleanCohorts());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [errorMessage]);

  const onSubmit = (data) => {
    const inputData = {
      ...data,
    };
    if (cohortId && cohortsData) {
      if (file == null) {
        dispatch(updateCohort(cohortId, inputData));
      } else if (file != null) {
        dispatch(uploadFile(file, cohortId));
        dispatch(updateCohort(cohortId, inputData));
      }
    } else {
      dispatch(addCohort(inputData, file));
    }
  };

  const cohortPhotoPreview = () => {
    if (cohortsState != null) {
      if (cohortsState.length === 0 && file == null) {
        return null;
      } else if (cohortsState.length !== 0 && file == null) {
        return (
          <img
            className="img-fluid rounded img-thumbnail full-width"
            style={{ height: '357px', objectFit: 'contain' }}
            src={cohortsState.location}
            alt="cohortPhoto"
          />
        );
      } else if (cohortsState.length !== 0 && file != null) {
        return (
          <img
            className="img-fluid rounded img-thumbnail full-width"
            style={{ height: '357px', objectFit: 'contain' }}
            src={previewImg}
            alt="cohortPhoto"
          />
        );
      } else if ((cohortsState == null || cohortsState.length === 0) && file != null) {
        return (
          <img
            className="img-fluid rounded img-thumbnail full-width"
            style={{ height: '357px', objectFit: 'contain' }}
            src={previewImg}
            alt="cohortPhoto"
          />
        );
      }
    } else {
      if ((cohortsState == null || cohortsState.length === 0) && file != null) {
        return (
          <img
            className="img-fluid rounded img-thumbnail full-width"
            style={{ height: '357px', objectFit: 'contain' }}
            src={previewImg}
            alt="cohortPhoto"
          />
        );
      }
    }
  };

  const onDeleteHandler = () => {
    const goBack = () => history.push('/adminview');
    DialogBox({
      id: cohortId,
      action: (cohort) => dispatch(deleteCohort(cohort)),
      errorMessage,
      text: 'If you delete this cohort all data will be deleted',
      confirmText: 'Delete',
      cancelText: 'Cancel',
      canceled: 'Canceled',
      canceledDelete: 'Data has not been deleted!',
      title: 'Are you sure?',
      goBack,
    });
  };

  const inputProps = {
    placeholder: 'Select Date',
    readOnly: true,
  };

  return (
    <ContentWrapper>
      <div className="content-heading d-flex justify-content-between">
        <div>{cohortId ? 'Edit' : 'Add Cohort'}</div>
        <Button
          onClick={() => history.goBack()}
          color="info"
          className="btn-labeled"
          style={{ display: 'flex', alignItems: 'center' }}
        >
          <span className="btn-label">
            <i className="fa fa-arrow-left" />
          </span>
          Go back
        </Button>
      </div>
      <Card className="col-12 col-md-12 col-xl-12 card b padding-col-mobile">
        <CardBody>
          <Row>
            <form style={{ width: '100%', display: 'flex', flexWrap: 'wrap' }}>
              <Col xl="6" md="12" style={{ overflow: 'hidden' }}>
                <FormGroup>
                  {/* <Dropzone
                    className="card b p-3"
                    onDrop={onDrop}
                    // accept="image/jpeg, image/png, image/jpe, image/jpg, image/bmp, application/pdf, .jpeg, .png, .jpe, .jpeg, .bmp, .pdf "
                    multiple={false}
                    style={{ cursor: 'pointer' }}
                  >
                    <div className="text-center box-placeholder m-0">
                      <p>Upload verification</p>
                      <p>{'Allowed types: jpg, jpe, png, jpeg!'}</p>
                    </div>
                    {/* <div>{file && file.name}</div>
                   </Dropzone> */}
                  <div className="card b p-3">
                    <div {...getRootProps({ style })}>
                      <input {...getInputProps()} />
                      <p>Upload verification</p>
                      <p>{'Allowed types: jpg, jpe, png, jpeg!'}</p>
                    </div>
                  </div>
                </FormGroup>
                {cohortPhotoPreview()}
              </Col>
              <Col xl="6" md="12">
                <FormGroup>
                  <label>Title</label>
                  <Controller
                    name="title"
                    control={control}
                    defaultValue=""
                    render={({ onChange, value }) => (
                      <Input name="title" value={value} onChange={onChange} placeholder="Title" />
                    )}
                  />
                  {errors.title && errors.title.type === 'required' && (
                    <span className="custom-invalid-input">This field is required</span>
                  )}
                </FormGroup>
                <FormGroup>
                  <label>Start Date</label>
                  <Controller
                    name="startDate"
                    control={control}
                    defaultValue=""
                    render={({ onChange, value }) => (
                      <Datetime
                        open={false}
                        placeholder="Select start date"
                        closeOnSelect
                        className="date-input"
                        inputProps={inputProps}
                        value={value}
                        onChange={onChange}
                        timeFormat={false}
                        dateFormat="DD-MM-YY"
                      />
                    )}
                  />
                  {errors.startDate && errors.startDate.type === 'required' && (
                    <span className="custom-invalid-input">This field is required</span>
                  )}
                </FormGroup>
                <FormGroup>
                  <label>End Date</label>
                  <Controller
                    name="endDate"
                    control={control}
                    defaultValue=""
                    render={({ onChange, value }) => (
                      <Datetime
                        placeholder="Select end date"
                        closeOnSelect
                        className="date-input"
                        inputProps={inputProps}
                        value={value}
                        onChange={onChange}
                        timeFormat={false}
                        dateFormat="DD-MM-YY"
                      />
                    )}
                  />
                  {errors.endDate && errors.endDate.type === 'required' && (
                    <span className="custom-invalid-input">This field is required</span>
                  )}
                </FormGroup>
                <FormGroup>
                  <label>Description</label>
                  <Controller
                    name="description"
                    control={control}
                    defaultValue=""
                    render={({ onChange, value }) => (
                      <Input
                        name="description"
                        type="textarea"
                        onChange={onChange}
                        value={value}
                        placeholder="Type description"
                      />
                    )}
                  />
                  {errors.description && errors.description.type === 'required' && (
                    <span className="custom-invalid-input">This field is required</span>
                  )}
                </FormGroup>

                <FormGroup>
                  <label>Text for button</label>
                  <Controller
                    name="textButton"
                    control={control}
                    defaultValue=""
                    render={({ onChange, value }) => (
                      <Input type="text" placeholder="Type text for button" onChange={onChange} value={value} />
                    )}
                  />
                  {errors.textButton && errors.textButton.type === 'required' && (
                    <span className="custom-invalid-input">This field is required</span>
                  )}
                </FormGroup>
                <FormGroup>
                  <label>Link for button</label>
                  <Controller
                    name="linkButton"
                    control={control}
                    defaultValue=""
                    render={({ onChange, value }) => (
                      <Input type="text" placeholder="Type text for button" onChange={onChange} value={value} />
                    )}
                  />
                  {errors.linkButton && errors.linkButton.type === 'required' && (
                    <span className="custom-invalid-input">This field is required</span>
                  )}
                </FormGroup>
              </Col>
            </form>
          </Row>

          {loading ? (
            <Spinner />
          ) : (
            <div className="text-right">
              <Button color="primary" onClick={handleSubmit(onSubmit)} type="submit" className="mr-1">
                {cohortId ? 'Modify' : 'Add'}
              </Button>
              {cohortId && (
                <>
                  <Button color="danger" onClick={handleSubmit(onDeleteHandler)} type="submit" className="ml-1">
                    Delete
                  </Button>
                </>
              )}
            </div>
          )}
        </CardBody>
      </Card>
    </ContentWrapper>
  );
}

export default EditAdminView;
