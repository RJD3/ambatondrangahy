/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react';
import DataTable from '../Common/DataTables';
import { Button } from 'reactstrap';
import { fetchCohorts, cleanCohorts } from '../../store/actions/actions';
//import {fetchCohorts, setDefault} from '../../store/actions';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { useToasts } from 'react-toast-notifications';
import DoneIcon from '@material-ui/icons/Done';

function AdminView() {
  const [cohortsDataTable, setCohortsDataTable] = useState([]);
  const [cohorts, setCohorts] = useState({});

  const state = useSelector((reduxState) => ({
    loading: reduxState.cohorts.loading,
    cohortsData: reduxState.cohorts.cohorts,
    errorMessage: reduxState.cohorts.error,
  }));

  const { loading, cohortsData, errorMessage } = state;
  const dispatch = useDispatch();
  const { addToast } = useToasts();

  useEffect(() => {
    if (cohortsData && cohortsData.data) {
      const cohortsArray = cohortsData.data.cohorts.docs;
      const dataT = cohortsArray.map((element) => {
        return [
          element.title,
          element.description,
          element.textButton,
          element.isCurrent ? (
            <Link to={`/editCohorts/${element._id}`} className='mr-1 mb-1'>
              <Button color='primary'>Edit</Button>
            </Link>
          ) : (
            <Link className='mr-1 mb-1'>
              <Button disabled={true} color='primary'>
                Edit
              </Button>
            </Link>
          ),
          <Link to={`/${element._id}/stats`} className='mr-1 mb-1'>
            <Button color='primary'>Stats</Button>
          </Link>,
          element.isCurrent ? (
            <DoneIcon style={{ marginLeft: '10px', color: '#117ff7' }} />
          ) : null,
        ];
      });
      setCohortsDataTable(dataT);
      setCohorts(cohortsData.data.cohorts);
    }
  }, [cohortsData]);

  useEffect(() => {
    dispatch(cleanCohorts());
    if (errorMessage) {
      addToast(errorMessage, {
        appearance: 'error',
        autoDismiss: true,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [errorMessage]);

  const columns = [
    'Title',
    'Description',
    'Text',
    'Action for edit',
    'Action for stats',
    'Current',
  ];

  const getPage = (page, term) => {
    if (page) {
      dispatch(fetchCohorts(page, term));
    }
  };

  return (
    <>
      <DataTable
        columns={columns}
        loading={loading}
        getPage={getPage}
        data={cohortsDataTable}
        // link='/editCohorts'
        fulldata={cohorts}
        hasAdd
        title='Cohorts'
        // textButton='Add Cohort'
      />
    </>
  );
}

export default AdminView;
