import React, { useState, useEffect, useMemo } from 'react';
import ApiService from '../../services/ApiService';
import moment from 'moment';
import { v4 } from 'uuid';
import { useToasts } from 'react-toast-notifications';
import { useDropzone } from 'react-dropzone';
import { useHistory } from 'react-router-dom';
import { Chip, Badge, Avatar } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { ArrowBack, NextWeek, Close } from '@material-ui/icons';
import { Button, Row, Col, Card, CardHeader, CardBody, FormGroup, Input } from 'reactstrap';

import ContentWrapper from '../Layout/ContentWrapper';
import { createGang, clearCreateGang } from '../../store/actions/actions';

const baseStyle = {
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '20px',
  borderWidth: 2,
  borderRadius: 2,
  borderColor: '#929292',
  borderStyle: 'dashed',
  backgroundColor: '#fafafa',
  color: '#929292',
  outline: 'none',
  transition: 'border .24s ease-in-out',
  cursor: 'pointer',
};
const thumbsContainer = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  marginTop: 16,
};
const dropzoneOptions = {
  multiple: true,
  maxFiles: 5,
  accept: {
    'image/*': [],
  },
};

const Create = (props) => {
  const [loading, setLoading] = useState(false);
  const [files, setFiles] = useState([]);

  const [code, setCode] = useState('');
  const [codeExpire, setCodeExpire] = useState('');
  const [codeUses, setCodeUses] = useState('');
  const [codeIsOffer, setCodeIsOffer] = useState(false);
  const [codes, setCodes] = useState([]);

  const [name, setName] = useState('');
  const [contactName, setContactName] = useState('');
  const [websiteUrl, setWebsiteUrl] = useState('');

  const createdResult = useSelector(({ gangs }) => gangs.create);

  const history = useHistory();
  const dispatch = useDispatch();
  const style = useMemo(() => ({ ...baseStyle }), []);
  const { getRootProps, getInputProps } = useDropzone({ ...dropzoneOptions, onDrop: (files) => addFiles(files) });
  const { addToast } = useToasts();

  const addCode = async (e) => {
    e.preventDefault();

    if (!code || !codeExpire || !codeUses) {
      addToast('Please fill all fields to create a new code!', { appearance: 'error', autoDismiss: true });
      return;
    }

    const now = new Date(Date.now());
    const expireDate = new Date(codeExpire);
    if (expireDate <= now) {
      addToast('Expire date can not be earlier than today!', { appearance: 'error', autoDismiss: true });
      return;
    }

    const minimumUses = 10;
    if (codeUses < minimumUses) {
      addToast(`Minimum number of uses is: ${minimumUses}!`, { appearance: 'error', autoDismiss: true });
      return;
    }

    const upperCaseCode = code.toUpperCase();
    const codeExists = codes.find((currentCode) => currentCode.value === upperCaseCode);
    if (codeExists) {
      addToast('Code with given value is already added!', { appearance: 'error', autoDismiss: true });
      return;
    }

    try {
      const result = await ApiService.get(`/gangcodes/${upperCaseCode}`);
      if (result.data && result.data.success) {
        const { gangCode } = result.data?.data;
        if (gangCode) {
          addToast('Code with given value is already added to another gang!', {
            appearance: 'error',
            autoDismiss: true,
          });
          return;
        }
      }
    } catch (error) {}

    const codePayload = {
      id: v4(),
      value: upperCaseCode,
      expiresAt: expireDate,
      isOfferCode: codeIsOffer,
      uses: codeUses,
    };
    const newCodes = [...codes, codePayload];
    setCodes(newCodes);

    clearCodeFields();
  };

  const removeCode = (id) => {
    const filteredCodes = codes.filter((code) => code.id !== id);
    setCodes(filteredCodes);
  };

  const clearCodeFields = () => {
    setCode('');
    setCodeExpire('');
    setCodeUses('');
    setCodeIsOffer(false);
  };

  const addFiles = (acceptedFiles) => {
    const newFiles = acceptedFiles.map((file) => Object.assign(file, { id: v4(), preview: URL.createObjectURL(file) }));
    setFiles([...files, ...newFiles]);
  };

  const removeFile = (id) => {
    const filteredFiles = files.filter((file) => file.id !== id);
    setFiles(filteredFiles);
  };

  const addGang = (e) => {
    e.preventDefault();

    if (!name || !contactName || !websiteUrl) {
      addToast('Please fill all fields to create a new code!', { appearance: 'error', autoDismiss: true });
      return;
    }

    if (!files || !files.length) {
      addToast('Please upload at least 1 badge!', { appearance: 'error', autoDismiss: true });
      return;
    }

    if (!codes || !codes.length) {
      addToast('Please add at least 1 code!', { appearance: 'error', autoDismiss: true });
      return;
    }

    const payload = {
      name,
      contactName,
      websiteUrl,
      codes,
      files,
    };
    dispatch(createGang(payload, { addToast, history }));
  };

  const handleCodeChange = (e) => setCode(e.target.value ? e.target.value.trim() : '');
  const handleCodeExpireChange = (e) => setCodeExpire(e.target.value);
  const handleCodeUsesChange = (e) => setCodeUses(e.target.value);
  const handleCodeIsOfferChange = (e) => setCodeIsOffer(e.target.value);

  const handleNameChange = (e) => setName(e.target.value);
  const handleContactNameChange = (e) => setContactName(e.target.value);
  const handleWebsiteUrlChange = (e) => setWebsiteUrl(e.target.value);

  useEffect(() => {
    if (createdResult) {
      switch (true) {
        case createdResult.loading:
          setLoading(true);
          break;
        case createdResult.success || createdResult.error:
          setLoading(false);
          break;
        default:
          break;
      }
    }
  }, [createdResult]);

  useEffect(() => {
    // Make sure to revoke the data uris to avoid memory leaks, will run on unmount
    return () => {
      dispatch(clearCreateGang());
      files.forEach((file) => URL.revokeObjectURL(file.preview));
    };
  }, []);

  return (
    <ContentWrapper>
      <div className="d-flex justify-content-between content-heading">
        <p>Gangs</p>
        <div>
          <Button
            onClick={() => history.goBack()}
            color="info"
            className="btn-labeled"
            style={{ display: 'flex', alignItems: 'center' }}
          >
            <span className="btn-label">
              {/* <ArrowBack style={{ fontSize: '17.5px' }} /> */}
              <i className="fas fa-arrow-left" />
            </span>
            Back
          </Button>
        </div>
      </div>

      <Row>
        <Col xs={12} md={6} xl={6}>
          <Card className="card-default" style={{ padding: '15px' }}>
            <section className="container">
              <div {...getRootProps({ style })}>
                <input {...getInputProps()} />
                <p>Drag and drop some badges here, or click to select badges...</p>
                <p>Maximum of 5 badges</p>
              </div>
              <aside style={thumbsContainer}>
                {files.map((file) => (
                  <Badge
                    key={file.id}
                    overlap="circle"
                    style={{ height: 100, width: 100, margin: '5px' }}
                    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                    badgeContent={
                      <Close
                        variant="contained"
                        onClick={() => {
                          removeFile(file.id);
                        }}
                        style={{
                          cursor: 'pointer',
                          fontSize: 30,
                          padding: '5px',
                          backgroundColor: '#ffffff',
                          boxShadow: '0px 0px 10px rgba(0,0,0,0.6)',
                          borderRadius: '50%',
                          marginBottom: 7,
                          marginLeft: 7,
                          color: 'black',
                        }}
                      />
                    }
                  >
                    <img
                      src={file.preview}
                      style={{ width: '100%', height: '100%', objectFit: 'contain' }}
                      // Revoke data uri after image is loaded
                      onLoad={() => {
                        URL.revokeObjectURL(file.preview);
                      }}
                    />
                  </Badge>
                ))}
              </aside>
            </section>
          </Card>

          <Card className="card-default">
            <CardBody>
              <form>
                <FormGroup>
                  <label>Code</label>
                  <Input
                    type="text"
                    placeholder="Code..."
                    disabled={loading}
                    value={code ? code.toUpperCase() : ''}
                    onChange={(e) => handleCodeChange(e)}
                  />
                </FormGroup>
                <FormGroup>
                  <label>Expire Date</label>
                  <Input
                    type="date"
                    disabled={loading}
                    value={codeExpire}
                    onChange={(e) => handleCodeExpireChange(e)}
                  />
                </FormGroup>
                <FormGroup>
                  <label>Number of Uses</label>
                  <Input
                    type="number"
                    placeholder="Number of Uses..."
                    disabled={loading}
                    value={codeUses}
                    onChange={(e) => handleCodeUsesChange(e)}
                  />
                </FormGroup>

                <FormGroup>
                  <label>Is this code an Apple Offer Code?</label>
                  <select
                    className="custom-select"
                    disabled={loading}
                    onChange={(e) => handleCodeIsOfferChange(e)}
                    value={codeIsOffer}
                  >
                    <option value={false}>No</option>
                    <option value={true}>Yes</option>
                  </select>
                </FormGroup>
                <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                  <button
                    className="btn btn-md btn-primary"
                    type="submit"
                    disabled={loading}
                    onClick={(e) => addCode(e)}
                  >
                    Add Code
                  </button>
                </div>
              </form>
            </CardBody>
          </Card>
        </Col>
        <Col xs={12} md={6} xl={6}>
          <Card className="card-default">
            <CardHeader>
              Any codes you add will appear here (click a code to make it the currently applied one)
            </CardHeader>
            <CardBody>
              {codes &&
                codes.map((code) => {
                  const { id, value, expiresAt, isOfferCode, uses } = code;
                  const codeType = isOfferCode ? 'GANG & OFFER CODE' : 'GANG CODE';
                  const label = `${value} | ${moment(expiresAt).format('MM-DD-YYYY')} | ${codeType} | ${uses}`;

                  return (
                    <Chip
                      color="primary"
                      style={{ margin: '5px' }}
                      key={id}
                      label={label}
                      disabled={loading}
                      onDelete={() => removeCode(id)}
                    />
                  );
                })}
            </CardBody>
          </Card>
          <Card className="card-default">
            <CardBody>
              <form>
                <FormGroup>
                  <label>Name</label>
                  <Input
                    type="text"
                    placeholder="Name..."
                    disabled={loading}
                    value={name}
                    onChange={(e) => handleNameChange(e)}
                  />
                </FormGroup>
                <FormGroup>
                  <label>Contact Name</label>
                  <Input
                    type="text"
                    placeholder="Contact Name..."
                    disabled={loading}
                    value={contactName}
                    onChange={(e) => handleContactNameChange(e)}
                  />
                </FormGroup>
                <FormGroup>
                  <label>Website Url</label>
                  <Input
                    type="text"
                    placeholder="Website Url..."
                    disabled={loading}
                    value={websiteUrl}
                    onChange={(e) => handleWebsiteUrlChange(e)}
                  />
                </FormGroup>
                <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                  <button
                    className="btn btn-md btn-primary"
                    type="submit"
                    disabled={loading}
                    onClick={(e) => addGang(e)}
                  >
                    Submit
                  </button>
                </div>
              </form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </ContentWrapper>
  );
};

export default Create;
