import React, { useState, useEffect } from 'react';
import Swal from 'sweetalert2';
import moment from 'moment';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useToasts } from 'react-toast-notifications';
import { Button } from 'reactstrap';
import { Edit, DeleteOutline } from '@material-ui/icons';

import ContentWrapper from '../Layout/ContentWrapper';
import MUIDataTable from 'mui-datatables';
import { getAllGangs, clearGetAllGangs, deleteOneGang, clearDeleteOneGang } from '../../store/actions/actions';

const getMUIColumns = () => ['Name', 'Contact Name', 'Website Url', 'Created At', 'Actions'];
const getMUIOptions = (onChangePage, onChangeRowsPerPage, onSearchChange, currentPage, currentLimit, count) => {
  return {
    selectableRows: 'none',
    print: false,
    filter: false,
    download: false,
    pagination: true,
    search: false,
    sort: false,
    serverSide: true,
    selectableRowsHeader: false,
    rowsPerPageOptions: [5, 10, 15],
    onChangePage: onChangePage,
    onChangeRowsPerPage: onChangeRowsPerPage,
    onSearchChange: onSearchChange,
    page: currentPage,
    rowsPerPage: currentLimit,
    count: count,
  };
};
const getMUIData = (gangs, dispatch, addToast, history) => {
  if (!gangs || (gangs && !gangs.length)) return [];

  return gangs.map((gang) => {
    const { _id, name, contactName, websiteUrl, createdAt } = gang;
    return [
      name,
      contactName,
      websiteUrl,
      moment(createdAt).format('YYYY/MM/DD'),
      <div>
        <Button
          color="primary"
          style={{ margin: '2.5px' }}
          onClick={() => {
            history.push(`/gangs/edit/${_id}`);
          }}
        >
          <Edit style={{ fontSize: '20px' }} />
        </Button>
        <Button
          color="danger"
          style={{ margin: '2.5px' }}
          onClick={() => {
            Swal.fire({
              title: 'Are you sure you want to delete this gang?',
              text: 'Deleting this gang will also delete all of its gang codes, badges and unjoin all users from that gang!',
              padding: '50px',
              showCancelButton: true,
              confirmButtonText: 'Yes, delete it!',
              confirmButtonColor: '#F05050',
              cancelButtonText: 'No, cancel it!',
            }).then((result) => {
              if (result.isConfirmed) dispatch(deleteOneGang({ gangId: _id }, { addToast }));
            });
          }}
        >
          <DeleteOutline style={{ fontSize: '20px' }} />
        </Button>
      </div>,
    ];
  });
};

const defaultPage = 0;
const defaultLimit = 10;

const List = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { addToast } = useToasts();

  const getAllResponse = useSelector(({ gangs }) => gangs.getAll);
  const deleteOneResponse = useSelector(({ gangs }) => gangs.deleteOne);

  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(defaultPage);
  const [limit, setLimit] = useState(defaultLimit);
  const [localGangs, setLocalGangs] = useState([]);
  const [gangsCount, setGangCount] = useState(0);

  useEffect(() => {
    const payload = { page: page + 1, limit: limit, pagination: true };
    dispatch(getAllGangs(payload));
  }, [page, limit]);

  useEffect(() => {
    if (deleteOneResponse && deleteOneResponse.success) {
      const payload = { page: defaultPage + 1, limit: defaultLimit, pagination: true };
      dispatch(getAllGangs(payload));

      setPage(defaultPage);
      setLimit(defaultLimit);
    }
  }, [deleteOneResponse]);

  useEffect(() => {
    if (getAllResponse) {
      switch (true) {
        case getAllResponse.loading:
          setLoading(true);
          break;
        case getAllResponse.success:
          const { docs, totalDocs } = getAllResponse.data.gangs;

          const muiGangs = getMUIData(docs, dispatch, addToast, history);
          setLocalGangs(muiGangs);
          setGangCount(totalDocs);

          setLoading(false);
          break;
        case getAllResponse.error:
          setLoading(false);
          break;
      }
    }
  }, [getAllResponse]);

  useEffect(() => {
    return () => {
      dispatch(clearGetAllGangs());
      dispatch(clearDeleteOneGang());
    };
  }, []);

  const onChangePage = (page) => {
    setPage(page);
  };

  const onChangeRowsPerPage = (limit) => {
    setPage(defaultPage);
    setLimit(limit);
  };

  return (
    <ContentWrapper>
      <div className="d-flex justify-content-between content-heading">
        <p>Gangs</p>
        <div>
          <Button
            onClick={() => history.push('/gangs/create')}
            color="info"
            className="btn-labeled"
            style={{ display: 'flex', alignItems: 'center' }}
          >
            <span className="btn-label">
              <i className="fas fa-plus" />
            </span>
            Create
          </Button>
        </div>
      </div>
      <MUIDataTable
        data={localGangs}
        columns={getMUIColumns()}
        options={getMUIOptions(onChangePage, onChangeRowsPerPage, null, page, limit, gangsCount)}
      />
    </ContentWrapper>
  );
};

export default List;
