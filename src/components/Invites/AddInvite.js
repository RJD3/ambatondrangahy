import React, { useEffect } from 'react';
import { Input, Button, Row, Col, FormGroup } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import { useForm, Controller } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useToasts } from 'react-toast-notifications';

import ContentWrapper from '../Layout/ContentWrapper';
import { addInvite, cleanInvites } from '../../store/actions/actions';

function AddInvite() {
  const { handleSubmit, errors, control, reset, register } = useForm();
  const history = useHistory();
  const dispatch = useDispatch();
  const { loading, added, error } = useSelector(({ invites }) => invites);
  const { addToast } = useToasts();

  useEffect(() => {
    dispatch(cleanInvites());

    return () => {
      dispatch(cleanInvites());
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (!added) return;

    addToast('Invite has been successfully sent!', {
      appearance: 'success',
      autoDismiss: true,
    });
    reset();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [added]);

  useEffect(() => {
    if (!error) return;

    addToast(error, {
      appearance: 'error',
      autoDismiss: true,
    });
    dispatch(cleanInvites());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [error]);

  useEffect(() => {
    register('invited', {
      required: true,
      pattern: {
        value: /^\(?\+?[\d\(\-\s\)]+$/,
        message: 'Phone number should be a valid number!',
      },
    });
  }, [register]);

  const onSubmitHandler = (data) => {
    dispatch(addInvite(data.invited, addToast));
  };

  return (
    <ContentWrapper>
      <div className='content-heading d-flex justify-content-between'>
        <p>Add Invite</p>
        <Button
          onClick={() => history.goBack()}
          color='info'
          className='btn-labeled'
          style={{ display: 'flex', alignItems: 'center' }}>
          <span className='btn-label'>
            <i className='fa fa-arrow-left' />
          </span>
          Back
        </Button>
      </div>
      <form onSubmit={handleSubmit(onSubmitHandler)}>
        <Row>
          <Col>
            <FormGroup>
              <Controller
                name='invited'
                control={control}
                defaultValue=''
                render={({ onChange, value }) => (
                  <Input
                    name='invited'
                    value={value}
                    onChange={onChange}
                    placeholder='Type Phone Number here...'
                  />
                )}
              />
              {errors.invited?.type === 'required' && (
                <span className='custom-invalid-input'>
                  This field is required
                </span>
              )}
              {errors.invited?.type === 'pattern' && (
                <span className='custom-invalid-input'>
                  {errors.invited.message}
                </span>
              )}
            </FormGroup>
          </Col>
          <Col>
            <Button color='primary' className='btn-large' type='submit'>
              {loading ? '. . .' : 'Invite'}
            </Button>
          </Col>
        </Row>
      </form>
    </ContentWrapper>
  );
}

export default AddInvite;
