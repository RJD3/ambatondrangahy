import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useToasts } from 'react-toast-notifications';
import moment from 'moment';

import Datatable from '../Common/DataTables';
import { getAllInvites, cleanInvites } from '../../store/actions/actions';

function Invites() {
  //   const [showModal, setShowModal] = useState(false);
  const [invitesDataTable, setInvitesDataTable] = useState([]);
  const { loading, error, invites, fullData } = useSelector(({ invites }) => invites);
  const dispatch = useDispatch();
  const { addToast } = useToasts();

  useEffect(() => {
    return () => {
      dispatch(cleanInvites());
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (!error) return;

    addToast(error, {
      appearance: 'error',
      autoDismiss: true,
    });
    dispatch(cleanInvites());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [error]);

  useEffect(() => {
    if (!invites) return;

    const dataT = invites.map((element) => {
      return [element.receiver, moment(element.createdAt).format('DD/MM/YYYY').toString()];
    });
    setInvitesDataTable(dataT);
  }, [invites]);

  const columns = ['Receiver', 'Sent Date'];

  const getPage = (page) => {
    if (page) dispatch(getAllInvites(page));
  };

  return (
    <>
      <Datatable
        columns={columns}
        loading={loading}
        getPage={getPage}
        data={invitesDataTable}
        fulldata={fullData}
        title='Invites'
        hasAdd
        link='/addinvite'
        textButton='Invite'
      />
    </>
  );
}

export default Invites;
