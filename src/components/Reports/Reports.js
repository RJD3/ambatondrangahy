import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { useToasts } from 'react-toast-notifications';
import moment from 'moment';
import { Button } from 'reactstrap';

import Datatable from '../Common/DataTables';
import { getAllReports, cleanReports } from '../../store/actions/actions';

function Reports() {
  const [reportsDataTable, setReportsDataTable] = useState([]);
  const [stateReports, setStateReports] = useState([]);
  const { loading, error, reports, fullData } = useSelector(
    ({ reports }) => reports
  );
  const dispatch = useDispatch();
  const { addToast } = useToasts();
  useEffect(() => {
    return () => {
      dispatch(cleanReports());
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (!error) return;

    addToast(error, {
      appearance: 'error',
      autoDismiss: true,
    });
    dispatch(cleanReports());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [error]);

  useEffect(() => {
    if (!reports) return;

    const dataT = reports.data.reports.docs.map((element) => {
      return [
        element.type,
        element.user ? element.user.username : 'NA',
        element.user ? element.user.phoneNumber : 'NA',
        moment(element.createdAt).format('DD/MM/YYYY').toString(),
        <Link to={`/reports/${element._id}`} className='mr-1 mb-1'>
          <Button color='primary'>View</Button>
        </Link>,
      ];
    });
    setReportsDataTable(dataT);
    setStateReports(reports.data.reports);
  }, [reports]);

  const columns = [
    'Report type',
    'Username',
    "User's phone number",
    'Reported at',
    'action',
  ];

  const getPage = (page) => {
    if (page) dispatch(getAllReports(page));
  };

  return (
    <>
      <Datatable
        columns={columns}
        loading={loading}
        getPage={getPage}
        data={reportsDataTable}
        fulldata={stateReports}
        title='Reports'
      />
    </>
  );
}

export default Reports;
