import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { useToasts } from 'react-toast-notifications';
import { Row, Col, Card, Button } from 'reactstrap';
import Player from 'react-player';
import moment from 'moment';

import ContentWrapper from '../Layout/ContentWrapper';
import {
  getOneReport,
  cleanReports,
  flagReport,
  cleanFlag,
} from '../../store/actions/actions';

function ReportDetailView() {
  const [playing, setPlaying] = useState(false);
  const [showFlagButton, setShowFlagButton] = useState(null);
  const { report, loadingFlag, flaged, error } = useSelector(
    ({ reports }) => reports
  );
  const { id } = useParams();
  const dispatch = useDispatch();
  const { addToast } = useToasts();
  console.log('report', report);
  useEffect(() => {
    if (!id) return;

    dispatch(getOneReport(id));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  useEffect(() => {
    if (!report) return;

    setShowFlagButton(report.post.isActive);
  }, [report]);

  useEffect(() => {
    if (!error) return;

    addToast(error, {
      appearance: 'error',
      autoDismiss: true,
    });
    dispatch(cleanReports());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [error]);

  useEffect(() => {
    if (!flaged) return;

    addToast('Post has been updated!', {
      appearance: 'success',
      autoDismiss: true,
    });
    dispatch(cleanFlag());
    setShowFlagButton((prevState) => !prevState);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [flaged]);

  const onPlayHandler = () => {
    setPlaying((prevState) => !prevState);
  };

  const onFlagHandler = () => {
    dispatch(flagReport(report?.post._id));
  };
  if (!report || !report.user) return null;
  return (
    <ContentWrapper>
      <Row className='px-3'>
        <Card className='b px-2 py-3'>
          <h4
            style={{
              marginLeft: '5px',
              marginBottom: '10px',
              marginTop: '10px',
            }}>
            Report
          </h4>
          <table style={{ tableLayout: 'fixed' }} className='table'>
            <tbody>
              <tr>
                <td>
                  <strong>Type:</strong>
                </td>
                <td>{report?.type}</td>
              </tr>
              <tr>
                <td>
                  <strong>Reported At:</strong>
                </td>
                <td>
                  {report && moment(report.createdAt).format('DD/MM/YYYY')}
                </td>
              </tr>
            </tbody>
          </table>
        </Card>
      </Row>
      <Row>
        <Col md={6}>
          <div className='' onClick={onPlayHandler}>
            <Player
              width='100%'
              url={
                report && report.post && report.post.video
                  ? report.post.video.location
                  : null
              }
              controls
              playing={playing}
            />
          </div>
        </Col>
        <Col md={6}>
          <Card className='b px-2 py-3'>
            <h4
              style={{
                marginLeft: '5px',
                marginBottom: '10px',
                marginTop: '10px',
              }}>
              User
            </h4>
            <table style={{ tableLayout: 'fixed' }} className='table'>
              <tbody>
                <tr>
                  <td>
                    <strong>Username:</strong>
                  </td>
                  <td>
                    {report && report.user && report.user.username
                      ? report.user.username
                      : ''}
                  </td>
                </tr>
                <tr>
                  <td>
                    <strong>Phone Number:</strong>
                  </td>
                  <td>
                    {report && report.user && report.user.phoneNumber
                      ? report.user.phoneNumber
                      : ''}
                  </td>
                </tr>
                <tr>
                  <td>
                    <strong>
                      {report && report.user && report.user.role == 3
                        ? 'Firm Name:'
                        : report.user.role == 4
                        ? 'Startup Name:'
                        : ''}
                    </strong>
                  </td>
                  <td>
                    {report && report.user && report.user.role == 3
                      ? report.user.firmName
                      : report.user.role == 4
                      ? report.user.startupName
                      : ''}
                  </td>
                </tr>
              </tbody>
            </table>
          </Card>
          <Row>
            <Col>
              <Button
                color='danger'
                block
                onClick={onFlagHandler}
                disabled={!showFlagButton}>
                {loadingFlag ? '. . .' : 'Flag'}
              </Button>
            </Col>
            <Col>
              <Button
                onClick={onFlagHandler}
                disabled={showFlagButton}
                color='success'
                block>
                {loadingFlag ? '. . .' : 'Unflag'}
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </ContentWrapper>
  );
}

export default ReportDetailView;
