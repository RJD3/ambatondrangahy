import React, { useState, useEffect } from 'react';
import { Input } from 'reactstrap';
import { doLogin, cleanLoginError ,cleanLoginSuccess} from '../../store/actions/actions';
import { useDispatch, useSelector } from 'react-redux';
import { useForm, Controller } from 'react-hook-form';
import { useHistory } from 'react-router-dom';

function Login() {
  const [errorMessage, setErrorMessage] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const dispatch = useDispatch();
  const { handleSubmit, control, errors, reset } = useForm();
  const history = useHistory();

  const loginError = useSelector(({ loginData }) => loginData.error);
  const token = useSelector(({ loginData }) => loginData.token);

  useEffect(() => {
    if (loginError) {
      setErrorMessage(loginError);
      setIsLoading(false);
      dispatch(cleanLoginError());
    } else {
      reset();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loginError]);

  useEffect(() => {
    dispatch(cleanLoginSuccess());
    if (token) {
      history.push('/adminview');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token]);

  const onSubmit = (data) => {
    setIsLoading(true);
    const { email, password } = data;
    dispatch(doLogin(email, password,history));
  };

  return (
    <div className={`block-center mt-4 wd-xl ${isLoading && 'standard whirl'}`}>
      <div className='card card-flat'>
        <div className='card-header text-center'>
          <a href='/login'>
            <img
              className='block-center img-fluid'
              src='img/gumPhoto.png'
              alt='Logo'
            />
          </a>
        </div>
        <div className='card-body'>
          <p className='text-center py-2'>LOGIN TO CONTINUE</p>
          {errorMessage !== '' && (
            <p className='text-center text-danger'>{errorMessage}</p>
          )}
          <form
            className='mb-3'
            name='formLogin'
            onSubmit={handleSubmit(onSubmit)}>
            <div className='form-group'>
              <div className='input-group with-focus'>
                <Controller
                  name='email'
                  control={control}
                  rules={{
                    required: true,
                    pattern: {
                      value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                      message: 'Format does not match email',
                    },
                  }}
                  defaultValue=''
                  render={({ onChange, value }) => (
                    <Input
                      onChange={onChange}
                      value={value}
                      type='email'
                      className='border-right-0'
                      placeholder='Email'
                    />
                  )}
                />
                <div className='input-group-append'>
                  <span className='input-group-text text-muted bg-transparent border-left-0'>
                    <em className='fa fa-envelope' />
                  </span>
                </div>
                {errors.email && errors.email.type === 'required' && (
                  <span className='custom-invalid-input'>
                    This field is required
                  </span>
                )}
                {errors.email && errors.email.type === 'pattern' && (
                  <span className='custom-invalid-input'>
                    {errors.email.message}
                  </span>
                )}
              </div>
            </div>
            <div className='form-group'>
              <div className='input-group with-focus'>
                <Controller
                  name='password'
                  control={control}
                  defaultValue=''
                  rules={{ required: true }}
                  render={({ onChange, value }) => (
                    <Input
                      type='password'
                      className='border-right-0'
                      placeholder='Password'
                      onChange={onChange}
                      value={value}
                    />
                  )}
                />
                <div className='input-group-append'>
                  <span className='input-group-text text-muted bg-transparent border-left-0'>
                    <em className='fa fa-lock' />
                  </span>
                </div>
                {errors.password && errors.password.type === 'required' && (
                  <span className='custom-invalid-input'>
                    This field is required
                  </span>
                )}
              </div>
            </div>
            <div className='clearfix'>
              {/* <CustomInput
                type="checkbox"
                id="rememberme"
                className="float-left mt-0"
                name="remember"
                label="Remember Me"
              /> */}
              {/* <div className="float-right">
                <Link to="recover" className="text-muted">
                  Forgot your password?
                </Link>
              </div> */}
            </div>
            <button
              className='btn btn-block mt-3'
              type='submit'
              style={{ backgroundColor: '#117FF7', color: 'white' }}>
              Login
            </button>
          </form>
        </div>
      </div>
      <div className='p-3 text-center'>
        <span className='mr-2'>&copy;</span>
        <span>2021</span>
        <span className='mx-2'>-</span>
        <span>Gum</span>
      </div>
    </div>
  );
}

export default Login;
