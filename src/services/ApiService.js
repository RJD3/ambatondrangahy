import axios from 'axios';
import Config from '../config';

const APIClient = axios.create({
  baseURL: Config.BASE_PATH,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});
APIClient.interceptors.request.use(
  (config) => {
    // const { token } = store.getState().loginData;
    const token = localStorage.getItem('token');
    if (token) {
      // eslint-disable-next-line no-param-reassign
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (err) => {
    return Promise.reject(err);
  }
);
export default APIClient;
