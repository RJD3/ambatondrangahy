const Config = {
  BASE_PATH: 'https://gum.appbit.es/api',
  // BASE_PATH: 'https://gum-staging.appbit.es/api',
  // BASE_PATH: 'http://192.168.0.42:5000/api',
};

export default Config;
