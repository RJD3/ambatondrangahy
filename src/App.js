/*!
 *
 * Angle - Bootstrap Admin Template
 *
 * Version: 4.7.8
 * Author: @themicon_co
 * Website: http://themicon.co
 * License: https://wrapbootstrap.com/help/licenses
 *
 */

import React, { useEffect, Suspense } from 'react';
import { ToastProvider } from 'react-toast-notifications';
import { useDispatch } from 'react-redux';
import {checkAuthState} from './store/actions/login.actions';
import Spinner from './components/Common/Spinner';
import config from './config.js';
import axios from 'axios';
// App Routes
import Routes from './Routes';

// Vendor dependencies
import "./Vendor";
// Application Styles
import './styles/bootstrap.scss';
import './styles/app.scss'


function App () {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(checkAuthState());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  axios.defaults.baseURL = config.BASE_PATH;
  

    return (
      <Suspense fallback={<Spinner style={{ top: '50%' }} />}>
           <ToastProvider autoDismissTimeout="2000">
            <Routes />
            </ToastProvider>
        </Suspense>
    );

  
}

export default App;
