import * as actionTypes from '../types/actionTypes';
import APIClient from '../../services/ApiService';

const getUsersStart = () => {
  return {
    type: actionTypes.GET_ALL_USERS_START,
  };
};

const getUsersSuccess = (payload) => {
  return {
    type: actionTypes.GET_ALL_USERS_SUCCESS,
    payload,
  };
};

const getUsersFailed = (error) => {
  return {
    type: actionTypes.GET_ALL_USERS_FAIL,
    error,
  };
};

export const fetchUsers = (payload) => {
  return (dispatch) => {
    dispatch(getUsersStart());
    const { page, limit, type } = payload;
    APIClient.get(
      `/users?page=${page}&limit=${limit}&type=${type.toUpperCase()}`
    )
      .then((res) => {
        if (res && res.data) {
          dispatch(getUsersSuccess(res.data));
        } else {
          dispatch(getUsersFailed(res.data.message));
        }
      })
      .catch((error) => {
        if (error.response && error.response.data.message) {
          dispatch(getUsersFailed(error.response.data.message));
        } else {
          dispatch(getUsersFailed('Unable to comunicate with the server.'));
        }
      });
  };
};

export const clearUsers = () => {
  return {
    type: actionTypes.CLEAN_USERS,
  };
};
