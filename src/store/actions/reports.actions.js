import * as actionTypes from '../types/actionTypes';
import ApiService from '../../services/ApiService';

function getAllReportsStart() {
  return {
    type: actionTypes.GET_ALL_REPORTS_START,
  };
}

function getAllReportsSuccess(payload) {
  return {
    type: actionTypes.GET_ALL_REPORTS_SUCCESS,
    payload,
  };
}

function getAllReportsFail(error) {
  return {
    type: actionTypes.GET_ALL_REPORTS_FAIL,
    error,
  };
}

function getOneReportStart() {
  return {
    type: actionTypes.GET_ONE_REPORT_START,
  };
}

function getOneReportSuccess(payload) {
  return {
    type: actionTypes.GET_ONE_REPORT_SUCCESS,
    payload,
  };
}

function getOneReportFail(error) {
  return {
    type: actionTypes.GET_ONE_REPORT_FAIL,
    error,
  };
}

function flagReportStart() {
  return {
    type: actionTypes.FLAG_REPORT_START,
  };
}

function flagReportSuccess() {
  return {
    type: actionTypes.FLAG_REPORT_SUCCESS,
  };
}

function flagReportFail(error) {
  return {
    type: actionTypes.FLAG_REPORT_FAIL,
    error,
  };
}

export const getAllReports = (page) => {
  return (dispatch) => {
    dispatch(getAllReportsStart());
    ApiService.get('/reports', {
      params: {
        page: page,
        limit: 10,
        paginate: true,
      },
    })
      .then(({ data }) => {
        if (!data.success)
          return dispatch(
            getAllReportsFail('Unable to get Reports! Please try later!')
          );

        dispatch(getAllReportsSuccess(data));
      })
      .catch((error) => {
        if (error.response?.data.error)
          return dispatch(getAllReportsFail(error.response.data.error));

        if (error.message) return dispatch(getAllReportsFail(error.message));
        dispatch(getAllReportsFail('Unable to comunicate with the server!'));
      });
  };
};

export const getOneReport = (id) => {
  return (dispatch) => {
    dispatch(getOneReportStart());
    ApiService.get(`/reports/${id}`)
      .then(({ data }) => {
        if (!data.success)
          return dispatch(
            getOneReportFail('Unable to communicate with the server!')
          );

        dispatch(getOneReportSuccess(data.data.report));
      })
      .catch((error) => {
        if (error.response?.data?.error)
          return dispatch(getOneReportFail(error.response.data.error));

        if (error.message) return dispatch(getOneReportFail(error.error));
        dispatch(getOneReportFail('Unable to comunicate with the server!'));
      });
  };
};

export const flagReport = (id) => {
  return (dispatch) => {
    dispatch(flagReportStart());
    ApiService.put('/reports/flag', { postId: id })
      .then(({ data }) => {
        if (!data.success)
          return dispatch(
            flagReportFail('Unable to flag post! Please try later!')
          );

        dispatch(flagReportSuccess());
      })
      .catch((error) => {
        if (error.response?.data.error)
          return dispatch(flagReportFail(error.response.data.error));

        if (error.message) return dispatch(flagReportFail(error.message));
        dispatch(flagReportFail('Unable to comunicate with the server!'));
      });
  };
};

export function cleanFlag() {
  return { type: actionTypes.CLEAN_FLAG };
}

export function cleanReports() {
  return {
    type: actionTypes.CLEAN_REPORTS,
  };
}
