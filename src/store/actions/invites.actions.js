import * as actionTypes from '../types/actionTypes';
import ApiService from '../../services/ApiService';

function getAllInvitesStart() {
  return {
    type: actionTypes.GET_ALL_INVITES_START,
  };
}

function getAllInvitesSuccess(payload, fullData) {
  return {
    type: actionTypes.GET_ALL_INVITES_SUCCESS,
    payload,
    fullData,
  };
}

function getAllInvitesFail(error) {
  return {
    type: actionTypes.GET_ALL_INVITES_FAIL,
    error,
  };
}

function addInviteStart() {
  return {
    type: actionTypes.ADD_INVITE_START,
  };
}

function addInviteSuccess(payload) {
  return {
    type: actionTypes.ADD_INVITE_SUCCESS,
    payload,
  };
}

function addInviteFail(error) {
  return {
    type: actionTypes.ADD_INVITE_FAIL,
    error,
  };
}

export const getAllInvites = (page) => {
  return (dispatch) => {
    dispatch(getAllInvitesStart());
    ApiService.get('/invites/admin', {
      params: {
        page,
        limit: 10,
      },
    })
      .then(({ data }) => {
        if (!data.success)
          return dispatch(
            getAllInvitesFail('Error getting all invites! Please try later!')
          );

        dispatch(
          getAllInvitesSuccess(data.data.invites.docs, data.data.invites)
        );
      })
      .catch((error) => {
        if (error.response?.data.error)
          return dispatch(getAllInvitesFail(error.response.data.error));

        if (error.message) return dispatch(getAllInvitesFail(error.message));
        dispatch(getAllInvitesFail('Unable to comunicate with the server!'));
      });
  };
};

export const addInvite = (invited, addToast) => {
  return (dispatch) => {
    dispatch(addInviteStart());
    ApiService.post('/invites/admin', { invited })
      .then(({ data }) => {
        if (!data.success) {
          return dispatch(
            addInviteFail('Error sending invite! Please try later!')
          );
        } else {
          addToast('Sent!', {
            appearance: 'success',
            autoDismiss: true,
          });
          dispatch(addInviteSuccess(data.data.user));
        }
      })
      .catch((error) => {
        if (error.response?.data.error)
          return dispatch(addInviteFail(error.response.data.error));

        if (error.message) return dispatch(addInviteFail(error.message));

        dispatch(addInviteFail('Error sending invite! Please try later!'));
      });
  };
};

export const cleanInvites = () => {
  return { type: actionTypes.CLEAN_INVITES };
};
