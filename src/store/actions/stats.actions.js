/* eslint-disable import/prefer-default-export */
import * as actionTypes from "../types/actionTypes";
import APIClient from "../../services/ApiService";

// ? =====GET ALL COHORTS ACTIONS=====

function getCohortUserStart() {
  return {
    type: actionTypes.GET_COHORT_USERS_START,
  };
}

function getCohortUsersFail(error) {
  return {
    type: actionTypes.GET_COHORT_USERS_FAIL,
    error,
  };
}

function getCohortUsersSuccess(responseData) {
  return {
    type: actionTypes.GET_COHORT_USERS_SUCCESS,
    responseData,
  };
}

// ? =========DISPATCH ACTIONS=============

// @desc    Get all cohorts.

export function fetchCohortUsers(id, page) {
  return (dispatch) => {
    dispatch(getCohortUserStart());
    APIClient.get(`/cohorts/${id}/stats`, {
      params: {
        page,
        limit: 10,
      },
    })
      .then((res) => {
        if (res.data.success) {
          dispatch(getCohortUsersSuccess(res.data));
        } else {
          dispatch(getCohortUsersFail(res.data.message));
        }
      })
      .catch((error) => {
        if (error.response && error.response.data.message) {
          dispatch(getCohortUsersFail(error.response.data.message));
        } else {
          dispatch(getCohortUsersFail("Unable to comunicate with the server."));
        }
      });
  };
}

export function fetchCohortUsersCurrent(page) {
  return (dispatch) => {
    dispatch(getCohortUserStart());
    APIClient.get(`/cohorts/current/stats`, {
      params: {
        page,
        limit: 10,
      },
    })
      .then((res) => {
        if (res.data.success) {
          dispatch(getCohortUsersSuccess(res.data));
        } else {
          dispatch(getCohortUsersFail(res.data.message));
        }
      })
      .catch((error) => {
        if (error.response && error.response.data.message) {
          dispatch(getCohortUsersFail(error.response.data.message));
        } else {
          dispatch(getCohortUsersFail("Unable to comunicate with the server."));
        }
      });
  };
}

export const cleanCohortUsers = () => {
  return (dispatch) => {
    dispatch({ type: actionTypes.SET_COHORT_USERS_DEFAULT });
  };
};
