import ApiService from '../../services/ApiService';
import { dispatchLoading, dispatchSuccess, dispatchError, getAxiosError } from '../../utils/redux';
import {
  GET_ALL_BADGES_START,
  GET_ALL_BADGES_SUCCESS,
  GET_ALL_BADGES_FAILED,
  GET_ALL_BADGES_RESET,
  GET_BADGE_USERS_START,
  GET_BADGE_USERS_SUCCESS,
  GET_BADGE_USERS_FAILED,
  GET_BADGE_USERS_RESET,
  GET_BADGE_CANDIDATES_START,
  GET_BADGE_CANDIDATES_SUCCESS,
  GET_BADGE_CANDIDATES_FAILED,
  GET_BADGE_CANDIDATES_RESET,
  GET_ONE_BADGE_START,
  GET_ONE_BADGE_SUCCESS,
  GET_ONE_BADGE_FAILED,
  GET_ONE_BADGE_RESET,
  CREATE_BADGE_START,
  CREATE_BADGE_SUCCESS,
  CREATE_BADGE_FAILED,
  CREATE_BADGE_RESET,
  ASSIGN_BADGE_START,
  ASSIGN_BADGE_SUCCESS,
  ASSIGN_BADGE_FAILED,
  ASSIGN_BADGE_RESET,
  REMOVE_BADGE_START,
  REMOVE_BADGE_SUCCESS,
  REMOVE_BADGE_FAILED,
  REMOVE_BADGE_RESET,
  UPDATE_ONE_BADGE_START,
  UPDATE_ONE_BADGE_SUCCESS,
  UPDATE_ONE_BADGE_FAILED,
  UPDATE_ONE_BADGE_RESET,
  DELETE_ONE_BADGE_START,
  DELETE_ONE_BADGE_SUCCESS,
  DELETE_ONE_BADGE_FAILED,
  DELETE_ONE_BADGE_RESET,
} from '../types/actionTypes';

const getAllBadgesStart = (payload) => ({ type: GET_ALL_BADGES_START, payload });
const getAllBadgesSuccess = (payload) => ({ type: GET_ALL_BADGES_SUCCESS, payload });
const getAllBadgesFailed = (payload) => ({ type: GET_ALL_BADGES_FAILED, payload });
const getAllBadgesReset = () => ({ type: GET_ALL_BADGES_RESET });

const getBadgeUsersStart = (payload) => ({ type: GET_BADGE_USERS_START, payload });
const getBadgeUsersSuccess = (payload) => ({ type: GET_BADGE_USERS_SUCCESS, payload });
const getBadgeUsersFailed = (payload) => ({ type: GET_BADGE_USERS_FAILED, payload });
const getBadgeUsersReset = (payload) => ({ type: GET_BADGE_USERS_RESET, payload });

const getBadgeCandidatesStart = (payload) => ({ type: GET_BADGE_CANDIDATES_START, payload });
const getBadgeCandidatesSuccess = (payload) => ({ type: GET_BADGE_CANDIDATES_SUCCESS, payload });
const getBadgeCandidatesFailed = (payload) => ({ type: GET_BADGE_CANDIDATES_FAILED, payload });
const getBadgeCandidatesReset = (payload) => ({ type: GET_BADGE_CANDIDATES_RESET, payload });

const getOneBadgeStart = (payload) => ({ type: GET_ONE_BADGE_START, payload });
const getOneBadgeSuccess = (payload) => ({ type: GET_ONE_BADGE_SUCCESS, payload });
const getOneBadgeFailed = (payload) => ({ type: GET_ONE_BADGE_FAILED, payload });
const getOneBadgeReset = () => ({ type: GET_ONE_BADGE_RESET });

const createBadgeStart = (payload) => ({ type: CREATE_BADGE_START, payload });
const createBadgeSuccess = (payload) => ({ type: CREATE_BADGE_SUCCESS, payload });
const createBadgeFailed = (payload) => ({ type: CREATE_BADGE_FAILED, payload });
const createBadgeReset = () => ({ type: CREATE_BADGE_RESET });

const assignBadgeStart = (payload) => ({ type: ASSIGN_BADGE_START, payload });
const assignBadgeSuccess = (payload) => ({ type: ASSIGN_BADGE_SUCCESS, payload });
const assignBadgeFailed = (payload) => ({ type: ASSIGN_BADGE_FAILED, payload });
const assignBadgeReset = () => ({ type: ASSIGN_BADGE_RESET });

const removeBadgeStart = (payload) => ({ type: REMOVE_BADGE_START, payload });
const removeBadgeSuccess = (payload) => ({ type: REMOVE_BADGE_SUCCESS, payload });
const removeBadgeFailed = (payload) => ({ type: REMOVE_BADGE_FAILED, payload });
const removeBadgeReset = () => ({ type: REMOVE_BADGE_RESET });

const updateOneBadgeStart = (payload) => ({ type: UPDATE_ONE_BADGE_START, payload });
const updateOneBadgeSuccess = (payload) => ({ type: UPDATE_ONE_BADGE_SUCCESS, payload });
const updateOneBadgeFailed = (payload) => ({ type: UPDATE_ONE_BADGE_FAILED, payload });
const updateOneBadgeReset = () => ({ type: UPDATE_ONE_BADGE_RESET });

const deleteOneBadgeStart = (payload) => ({ type: DELETE_ONE_BADGE_START, payload });
const deleteOneBadgeSuccess = (payload) => ({ type: DELETE_ONE_BADGE_SUCCESS, payload });
const deleteOneBadgeFailed = (payload) => ({ type: DELETE_ONE_BADGE_FAILED, payload });
const deleteOneBadgeReset = () => ({ type: DELETE_ONE_BADGE_RESET });

// Get All Badges.
export const getAllBadges = (payload) => {
  return async (dispatch) => {
    try {
      dispatchLoading(dispatch, getAllBadgesStart);

      const { page, limit, pagination } = payload;
      const params = { page, limit, paginate: pagination };

      const result = await ApiService.get('/badges', { params });
      if (result.data && result.data.success) {
        const { badges } = result.data?.data;
        dispatchSuccess(dispatch, getAllBadgesSuccess, badges, 'badges');
      } else {
        const errorMessage = result.data?.error || 'Something went wrong!';
        dispatchError(dispatch, getAllBadgesFailed, errorMessage);
      }
    } catch (error) {
      const errorMessage = getAxiosError(error);
      dispatchError(dispatch, getAllBadgesFailed, errorMessage);
    }
  };
};

export const clearGetAllBadges = () => getAllBadgesReset();

// Get Badge Users.
export const getBadgeUsers = (payload) => {
  return async (dispatch) => {
    try {
      dispatchLoading(dispatch, getBadgeUsersStart);

      const { badgeId, page, limit, pagination, search } = payload;
      const params = { page, limit, paginate: pagination, search };

      const result = await ApiService.get(`/badges/${badgeId}/users`, { params });
      if (result.data && result.data.success) {
        const { users } = result.data?.data;
        dispatchSuccess(dispatch, getBadgeUsersSuccess, users, 'users');
      } else {
        const errorMessage = result.data?.error || 'Something went wrong!';
        dispatchError(dispatch, getBadgeUsersFailed, errorMessage);
      }
    } catch (error) {
      const errorMessage = getAxiosError(error);
      dispatchError(dispatch, getBadgeUsersFailed, errorMessage);
    }
  };
};

export const clearGetBadgeUsers = () => getBadgeUsersReset();

// Get Badge Candidates.
export const getBadgeCandidates = (payload) => {
  return async (dispatch) => {
    try {
      dispatchLoading(dispatch, getBadgeCandidatesStart);

      const { badgeId, page, limit, pagination, search } = payload;
      const params = { page, limit, paginate: pagination, search };

      const result = await ApiService.get(`/badges/${badgeId}/candidates`, { params });
      if (result.data && result.data.success) {
        const { users } = result.data?.data;
        dispatchSuccess(dispatch, getBadgeCandidatesSuccess, users, 'users');
      } else {
        const errorMessage = result.data?.error || 'Something went wrong!';
        dispatchError(dispatch, getBadgeCandidatesFailed, errorMessage);
      }
    } catch (error) {
      const errorMessage = getAxiosError(error);
      dispatchError(dispatch, getBadgeCandidatesFailed, errorMessage);
    }
  };
};

export const clearGetBadgeCandidates = () => getBadgeCandidatesReset();

// Get One Badge.
export const getOneBadge = (payload) => {
  return async (dispatch) => {
    try {
      dispatchLoading(dispatch, getOneBadgeStart);

      const { badgeId } = payload;

      const result = await ApiService.get(`/badges/${badgeId}`);
      if (result.data && result.data.success) {
        const { badge } = result.data?.data;
        dispatchSuccess(dispatch, getOneBadgeSuccess, badge, 'badge');
      } else {
        const errorMessage = result.data?.error || 'Something went wrong!';
        dispatchError(dispatch, getOneBadgeFailed, errorMessage);
      }
    } catch (error) {
      const errorMessage = getAxiosError(error);
      dispatchError(dispatch, getOneBadgeFailed, errorMessage);
    }
  };
};

export const clearGetOneBadge = () => getOneBadgeReset();

// Create Badge.
export const createBadge = (payload, options = {}) => {
  const { addToast, history } = options;
  return async (dispatch) => {
    try {
      dispatchLoading(dispatch, createBadgeStart);

      const { name, description, file } = payload;

      const formData = new FormData();
      formData.append('name', name);
      formData.append('description', description);
      formData.append('photo', file);

      const result = await ApiService.post(`/badges`, formData);
      if (result.data && result.data.success) {
        const { badge } = result.data?.data;
        dispatchSuccess(dispatch, createBadgeSuccess, badge, 'badge');
        addToast('Badge created successfully!', { appearance: 'success', autoDismiss: true });
        history.push('/badges');
      } else {
        const errorMessage = result.data?.error || 'Something went wrong!';
        dispatchError(dispatch, createBadgeFailed, errorMessage);
        addToast(errorMessage, { appearance: 'error', autoDismiss: true });
      }
    } catch (error) {
      const errorMessage = getAxiosError(error);
      addToast(errorMessage, { appearance: 'error', autoDismiss: true });
      dispatchError(dispatch, createBadgeFailed, errorMessage);
    }
  };
};

export const clearCreateBadge = (payload) => createBadgeReset();

// Assign Badge To User.
export const assignBadge = (payload, options = {}) => {
  const { addToast, history } = options;
  return async (dispatch) => {
    try {
      dispatchLoading(dispatch, assignBadgeStart);

      const { badgeId, userId } = payload;

      const result = await ApiService.post(`/badges/${badgeId}/assign/${userId}`, {});
      if (result.data && result.data.success) {
        const { user } = result.data?.data;
        dispatchSuccess(dispatch, assignBadgeSuccess, user, 'user');
        addToast('Badge assigned to user successfully!', { appearance: 'success', autoDismiss: true });
      } else {
        const errorMessage = result.data?.error || 'Something went wrong!';
        dispatchError(dispatch, assignBadgeFailed, errorMessage);
        addToast(errorMessage, { appearance: 'error', autoDismiss: true });
      }
    } catch (error) {
      const errorMessage = getAxiosError(error);
      addToast(errorMessage, { appearance: 'error', autoDismiss: true });
      dispatchError(dispatch, assignBadgeFailed, errorMessage);
    }
  };
};

export const clearAssignBadge = (payload) => assignBadgeReset();

// Remove Badge From User.
export const removeBadge = (payload, options = {}) => {
  const { addToast, history } = options;
  return async (dispatch) => {
    try {
      dispatchLoading(dispatch, removeBadgeStart);

      const { badgeId, userId } = payload;

      const result = await ApiService.post(`/badges/${badgeId}/remove/${userId}`, {});
      if (result.data && result.data.success) {
        const { user } = result.data?.data;
        dispatchSuccess(dispatch, removeBadgeSuccess, user, 'user');
        addToast('Badge removed from user successfully!', { appearance: 'success', autoDismiss: true });
      } else {
        const errorMessage = result.data?.error || 'Something went wrong!';
        dispatchError(dispatch, removeBadgeFailed, errorMessage);
        addToast(errorMessage, { appearance: 'error', autoDismiss: true });
      }
    } catch (error) {
      const errorMessage = getAxiosError(error);
      addToast(errorMessage, { appearance: 'error', autoDismiss: true });
      dispatchError(dispatch, removeBadgeFailed, errorMessage);
    }
  };
};

export const clearRemoveBadge = (payload) => removeBadgeReset();

// Update One Badge.
export const updateOneBadge = (payload, options = {}) => {
  const { addToast, history } = options;
  return async (dispatch) => {
    try {
      dispatchLoading(dispatch, updateOneBadgeStart);

      const { name, description, file } = payload;
      const { badgeId } = payload;

      const formData = new FormData();

      formData.append('name', name);
      formData.append('description', description);
      if (file) formData.append('photo', file);

      const result = await ApiService.put(`/badges/${badgeId}`, formData);
      if (result.data && result.data.success) {
        const { badge } = result.data?.data;
        dispatchSuccess(dispatch, updateOneBadgeSuccess, badge, 'badge');
        addToast('Badge updated successfully!', { appearance: 'success', autoDismiss: true });
        history.push('/badges');
      } else {
        const errorMessage = result.data?.error || 'Something went wrong!';
        dispatchError(dispatch, updateOneBadgeFailed, errorMessage);
        addToast(errorMessage, { appearance: 'error', autoDismiss: true });
      }
    } catch (error) {
      const errorMessage = getAxiosError(error);
      dispatchError(dispatch, updateOneBadgeFailed, errorMessage);
      addToast(errorMessage, { appearance: 'error', autoDismiss: true });
    }
  };
};

export const clearUpdateOneBadge = (payload) => updateOneBadgeReset();

// Delete One Badge.
export const deleteOneBadge = (payload, options = {}) => {
  const { addToast } = options;
  return async (dispatch) => {
    try {
      dispatchLoading(dispatch, deleteOneBadgeStart);

      const { badgeId } = payload;

      const result = await ApiService.delete(`/badges/${badgeId}`);
      if (result.data && result.data.success) {
        const { badge } = result.data?.data;
        dispatchSuccess(dispatch, deleteOneBadgeSuccess, badge, 'badge');
        addToast('Badge deleted successfully!', { appearance: 'success', autoDismiss: true });
      } else {
        const errorMessage = result.data?.error || 'Something went wrong!';
        dispatchError(dispatch, deleteOneBadgeFailed, errorMessage);
        addToast(errorMessage, { appearance: 'error', autoDismiss: true });
      }
    } catch (error) {
      const errorMessage = getAxiosError(error);
      dispatchError(dispatch, deleteOneBadgeFailed, errorMessage);
      addToast(errorMessage, { appearance: 'error', autoDismiss: true });
    }
  };
};

export const clearDeleteOneBadge = () => deleteOneBadgeReset();
