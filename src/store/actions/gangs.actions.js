import ApiService from '../../services/ApiService';
import { dispatchLoading, dispatchSuccess, dispatchError, getAxiosError } from '../../utils/redux';
import {
  GET_ALL_GANGS_START,
  GET_ALL_GANGS_SUCCESS,
  GET_ALL_GANGS_FAILED,
  GET_ALL_GANGS_RESET,
  GET_ONE_GANG_START,
  GET_ONE_GANG_SUCCESS,
  GET_ONE_GANG_FAILED,
  GET_ONE_GANG_RESET,
  CREATE_GANG_START,
  CREATE_GANG_SUCCESS,
  CREATE_GANG_FAILED,
  CREATE_GANG_RESET,
  UPDATE_ONE_GANG_START,
  UPDATE_ONE_GANG_SUCCESS,
  UPDATE_ONE_GANG_FAILED,
  UPDATE_ONE_GANG_RESET,
  DELETE_ONE_GANG_START,
  DELETE_ONE_GANG_SUCCESS,
  DELETE_ONE_GANG_FAILED,
  DELETE_ONE_GANG_RESET,
} from '../types/actionTypes';

const getAllGangsStart = (payload) => ({ type: GET_ALL_GANGS_START, payload });
const getAllGangsSuccess = (payload) => ({ type: GET_ALL_GANGS_SUCCESS, payload });
const getAllGangsFailed = (payload) => ({ type: GET_ALL_GANGS_FAILED, payload });
const getAllGangsReset = () => ({ type: GET_ALL_GANGS_RESET });

const getOneGangStart = (payload) => ({ type: GET_ONE_GANG_START, payload });
const getOneGangSuccess = (payload) => ({ type: GET_ONE_GANG_SUCCESS, payload });
const getOneGangFailed = (payload) => ({ type: GET_ONE_GANG_FAILED, payload });
const getOneGangReset = () => ({ type: GET_ONE_GANG_RESET });

const createGangStart = (payload) => ({ type: CREATE_GANG_START, payload });
const createGangSuccess = (payload) => ({ type: CREATE_GANG_SUCCESS, payload });
const createGangFailed = (payload) => ({ type: CREATE_GANG_FAILED, payload });
const createGangReset = () => ({ type: CREATE_GANG_RESET });

const updateOneGangStart = (payload) => ({ type: UPDATE_ONE_GANG_START, payload });
const updateOneGangSuccess = (payload) => ({ type: UPDATE_ONE_GANG_SUCCESS, payload });
const updateOneGangFailed = (payload) => ({ type: UPDATE_ONE_GANG_FAILED, payload });
const updateOneGangReset = () => ({ type: UPDATE_ONE_GANG_RESET });

const deleteOneGangStart = (payload) => ({ type: DELETE_ONE_GANG_START, payload });
const deleteOneGangSuccess = (payload) => ({ type: DELETE_ONE_GANG_SUCCESS, payload });
const deleteOneGangFailed = (payload) => ({ type: DELETE_ONE_GANG_FAILED, payload });
const deleteOneGangReset = () => ({ type: DELETE_ONE_GANG_RESET });

// Get All Gangs.
export const getAllGangs = (payload) => {
  return async (dispatch) => {
    try {
      dispatchLoading(dispatch, getAllGangsStart);

      const { page, limit, pagination } = payload;
      const params = { page, limit, pagination };

      const result = await ApiService.get('/gangs', { params });
      if (result.data && result.data.success) {
        const { gangs } = result.data?.data;
        dispatchSuccess(dispatch, getAllGangsSuccess, gangs, 'gangs');
      } else {
        const errorMessage = result.data?.error || 'Something went wrong!';
        dispatchError(dispatch, getAllGangsFailed, errorMessage);
      }
    } catch (error) {
      const errorMessage = getAxiosError(error);
      dispatchError(dispatch, getAllGangsFailed, errorMessage);
    }
  };
};

export const clearGetAllGangs = () => getAllGangsReset();

// Get One Gang.
export const getOneGang = (payload) => {
  return async (dispatch) => {
    try {
      dispatchLoading(dispatch, getOneGangStart);

      const { gangId } = payload;

      const result = await ApiService.get(`/gangs/${gangId}`);
      if (result.data && result.data.success) {
        const { gang } = result.data?.data;
        dispatchSuccess(dispatch, getOneGangSuccess, gang, 'gang');
      } else {
        const errorMessage = result.data?.error || 'Something went wrong!';
        dispatchError(dispatch, getOneGangFailed, errorMessage);
      }
    } catch (error) {
      const errorMessage = getAxiosError(error);
      dispatchError(dispatch, getOneGangFailed, errorMessage);
    }
  };
};

export const clearGetOneGang = () => getOneGangReset();

// Create Gang.
export const createGang = (payload, options = {}) => {
  const { addToast, history } = options;
  return async (dispatch) => {
    try {
      dispatchLoading(dispatch, createGangStart);

      const formData = new FormData();

      const fields = [
        { value: 'name', isArray: false, isFile: false },
        { value: 'contactName', isArray: false, isFile: false },
        { value: 'codes', isArray: true, isFile: false },
        { value: 'websiteUrl', isArray: false, isFile: false },
        { value: 'files', isArray: true, isFile: true },
      ];

      fields.forEach(({ value, isArray, isFile }) => {
        if (isArray && isFile) payload[value].forEach((val) => formData.append(val.id, val));
        else if (isArray && !isFile) formData.append(value, JSON.stringify(payload[value]));
        else formData.append(value, payload[value]);
      });

      const result = await ApiService.post(`/gangs`, formData);
      if (result.data && result.data.success) {
        const { gang } = result.data?.data;
        dispatchSuccess(dispatch, createGangSuccess, gang, 'gang');
        addToast('Gang created successfully!', { appearance: 'success', autoDismiss: true });
        history.push('/gangs');
      } else {
        const errorMessage = result.data?.error || 'Something went wrong!';
        dispatchError(dispatch, createGangFailed, errorMessage);
        addToast(errorMessage, { appearance: 'error', autoDismiss: true });
      }
    } catch (error) {
      const errorMessage = getAxiosError(error);
      addToast(errorMessage, { appearance: 'error', autoDismiss: true });
      dispatchError(dispatch, createGangFailed, errorMessage);
    }
  };
};

export const clearCreateGang = (payload) => createGangReset();

// Update One Gang.
export const updateOneGang = (payload, options = {}) => {
  const { addToast, history } = options;
  return async (dispatch) => {
    try {
      dispatchLoading(dispatch, updateOneGangStart);

      const { gangId } = payload;

      const formData = new FormData();

      const fields = [
        { value: 'name', isArray: false, isFile: false },
        { value: 'contactName', isArray: false, isFile: false },
        { value: 'codes', isArray: true, isFile: false },
        { value: 'websiteUrl', isArray: false, isFile: false },
        { value: 'files', isArray: true, isFile: true },
        { value: 'toBeDeleted', isArray: true, isFile: false },
        { value: 'codesToDelete', isArray: true, isFile: false },
      ];

      fields.forEach(({ value, isArray, isFile }) => {
        if (isArray && isFile) payload[value].forEach((val) => formData.append(val.id, val));
        else if (isArray && !isFile) formData.append(value, JSON.stringify(payload[value]));
        else formData.append(value, payload[value]);
      });

      const result = await ApiService.put(`/gangs/${gangId}`, formData);
      if (result.data && result.data.success) {
        const { gang } = result.data?.data;
        dispatchSuccess(dispatch, updateOneGangSuccess, gang, 'gang');
        addToast('Gang updated successfully!', { appearance: 'success', autoDismiss: true });
        history.push('/gangs');
      } else {
        const errorMessage = result.data?.error || 'Something went wrong!';
        dispatchError(dispatch, updateOneGangFailed, errorMessage);
        addToast(errorMessage, { appearance: 'error', autoDismiss: true });
      }
    } catch (error) {
      const errorMessage = getAxiosError(error);
      dispatchError(dispatch, updateOneGangFailed, errorMessage);
      addToast(errorMessage, { appearance: 'error', autoDismiss: true });
    }
  };
};

export const clearUpdateOneGang = (payload) => updateOneGangReset();

// Delete One Gang.
export const deleteOneGang = (payload, options = {}) => {
  const { addToast } = options;
  return async (dispatch) => {
    try {
      dispatchLoading(dispatch, deleteOneGangStart);

      const { gangId } = payload;

      const result = await ApiService.delete(`/gangs/${gangId}`);
      if (result.data && result.data.success) {
        const { gang } = result.data?.data;
        dispatchSuccess(dispatch, deleteOneGangSuccess, gang, 'gang');
        addToast('Gang deleted successfully!', { appearance: 'success', autoDismiss: true });
      } else {
        const errorMessage = result.data?.error || 'Something went wrong!';
        dispatchError(dispatch, deleteOneGangFailed, errorMessage);
        addToast(errorMessage, { appearance: 'error', autoDismiss: true });
      }
    } catch (error) {
      const errorMessage = getAxiosError(error);
      dispatchError(dispatch, deleteOneGangFailed, errorMessage);
      addToast(errorMessage, { appearance: 'error', autoDismiss: true });
    }
  };
};

export const clearDeleteOneGang = () => deleteOneGangReset();
