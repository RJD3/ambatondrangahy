import APIClient from '../../services/ApiService';
import * as actionTypes from '../types/actionTypes';
import jwt from 'jsonwebtoken';

// ? =====CREATE CONFIRMATION ACTIONS=======

function loginStart() {
  return {
    type: actionTypes.LOGIN_START,
  };
}

function loginFail(error) {
  return {
    type: actionTypes.LOGIN_FAIL,
    error,
  };
}

function loginSuccess(payload){
  return {
    type:actionTypes.LOGIN_SUCCESS,
    payload
  }
}

function confirmSuccess(token) {
  return {
    type: actionTypes.CONFIRM_SUCCESS,
    token,
    error: '',
  };
}

export function loginData(name, value) {
  return { type: name, value };
}

export const doLogout = () => (dispatch) => {
  dispatch({
    type: actionTypes.LOG_OUT,
  });
  localStorage.removeItem('token');
  localStorage.removeItem('expirationDate');
  // window.location.replace('/login');
};

export function checkAuthTimeout(expirationTime) {
  return (dispatch) => {
    setTimeout(() => {
      dispatch(doLogout());
    }, expirationTime * 1000);
  };
}

// @desc    Login.

export function doLogin(email, password, history) {
  return (dispatch) => {
    dispatch(loginStart());
    APIClient.post('/auth/login/admin', {
      email,
      password,
    })
      .then((res) => {
        if (res.data.success) {
          dispatch(loginSuccess(res.data.success))
          history.push('/confirm');
        } else {
          dispatch(loginFail(res.data.message));
        }
      })
      .catch((error) => {
        if (error.response && error.response.data.message) {
          dispatch(loginFail(error.response.data.message));
        } else {
          dispatch(loginFail('Unable to comunicate with the server.'));
        }
      });
  };
}

export function doConfirm (code,addToast) {
  return (dispatch) => {
    APIClient.post(`/auth/login/admin/confirm`,{code})
    .then((res)=> {
      if(res.data.success){
        addToast('Logged in successfully',{
          appearance: 'success',
          autoDismiss: true,
        })
        const { token } = res.data.data;
        const { exp } = jwt.decode(token);
        const expiresIn = Math.abs(Date.now() - exp * 1000);
        const expirationDate = new Date(Date.now());

        dispatch(confirmSuccess(token));
        dispatch({ type: 'TOKEN', value: token });
        dispatch(checkAuthTimeout(expiresIn));
        localStorage.setItem('token', token);
        localStorage.setItem('expirationDate', expirationDate);

      }else {
        addToast('Something went wrong',{
          appearance: 'error',
          autoDismiss: true,
        })
      }
    })
    .catch((error)=> {
      
      if (error.response && error.response.data.error) {
        addToast(error.response.data.error,{
          appearance:'error',
          autoDismiss:true
        })
      } else {
        addToast('Unable to comunicate with the server.',{
          appearance:'error',
          autoDismiss:true
        })
      }
    })
  }
}

export function doResend(addToast){
  return (dispatch) => {
    APIClient.post(`/auth/login/admin/resend`)
    .then((res)=> {
      if(res.data.success){

      }else {
        addToast('Something went wrong',{
          appearance: 'error',
          autoDismiss: true,
        })
      }
    })
    .catch((error)=> {
      if (error.response && error.response.data.error) {
        addToast(error.response.data.error,{
          appearance:'error',
          autoDismiss:true
        })
      } else {
        addToast('Unable to comunicate with the server.',{
          appearance:'error',
          autoDismiss:true
        })
      }
    })
  }
}

export function cleanLoginSuccess(){
  return {
    type:actionTypes.CLEAN_LOGIN_SUCCESS
  }
}

export function cleanLoginError() {
  return {
    type: actionTypes.CLEAN_LOGIN_ERROR,
  };
}

export const clearData = () => {
  return {
    type: actionTypes.CLEAR,
  };
};

export function checkAuthState() {
  return (dispatch) => {
    const token = localStorage.getItem('token');
    if (!token) {
      dispatch(doLogout());
    } else {
      const expirationDate = new Date(localStorage.getItem('expirationDate'));
      if (expirationDate < new Date()) {
        const expiresIn = Date.now() - expirationDate.getTime();
        dispatch(confirmSuccess(token));
        dispatch(checkAuthTimeout(expiresIn));
      } else {
        dispatch(doLogout());
      }
    }
  };
}