/* eslint-disable import/prefer-default-export */
import * as actionTypes from '../types/actionTypes';
import APIClient from '../../services/ApiService';

// ? =====GET ALL COHORTS ACTIONS=====

function getCohortsStart() {
  return {
    type: actionTypes.GET_COHORTS_START,
  };
}

function getCohortsFail(error) {
  return {
    type: actionTypes.GET_COHORTS_FAIL,
    error,
  };
}

function getCohortsSuccess(responseData) {
  return {
    type: actionTypes.GET_COHORTS_SUCCESS,
    responseData,
  };
}

// ? ====GET SINGLE COHORT ACTIONS====

function getSingleCohortStart() {
  return {
    type: actionTypes.GET_COHORT_START,
  };
}

function getSingleCohortFail(error) {
  return {
    type: actionTypes.GET_COHORT_FAIL,
    error,
  };
}

function getSingleCohortSuccess(fetched) {
  return {
    type: actionTypes.GET_COHORT_SUCCESS,
    fetched,
  };
}

// ? ==========CREATE COHORTS ACTIONS============
function addCohortStart() {
  return {
    type: actionTypes.ADD_COHORT_START,
  };
}

function addCohortFail(error) {
  return {
    type: actionTypes.ADD_COHORT_FAIL,
    error,
  };
}

function addCohortSuccess(added) {
  return {
    type: actionTypes.ADD_COHORT_SUCCESS,
    added,
  };
}
//? ============UPDATE COHORT ACTIONS==========
function updateCohortStart() {
  return {
    type: actionTypes.EDIT_COHORT_START,
  };
}
function updateCohortFail(error) {
  return {
    type: actionTypes.EDIT_COHORT_FAIL,
    error,
  };
}
function updateCohortSuccess(updated) {
  return {
    type: actionTypes.EDIT_COHORT_SUCCESS,
    updated,
  };
}

// ? =====DELETE COHORT ACTIONS=======

function deleteCohortStart() {
  return {
    type: actionTypes.DELETE_COHORT_START,
  };
}

function deleteCohortFail(error) {
  return {
    type: actionTypes.DELETE_COHORT_FAIL,
    error,
  };
}

function deleteCohortSuccess(deleted) {
  return {
    type: actionTypes.DELETE_COHORT_SUCCESS,
    deleted,
  };
}

// ? =====UPLOAD FILE ACTIONS=======

function uploadFileStart() {
  return {
    type: actionTypes.UPLOAD_FILE_START,
  };
}

function uploadFileFail(error) {
  return {
    type: actionTypes.UPLOAD_FILE_FAIL,
    error,
  };
}

function uploadFileSuccess(fileURL) {
  return {
    type: actionTypes.UPLOAD_FILE_SUCCESS,
    fileURL,
  };
}

// ?=======set upload percentage===========

function setUploadPercentage(percentage) {
  return {
    type: actionTypes.SET_UPLOAD_PERCENTAGE,
    percentage,
  };
}

export const deleteFileUrl = () => {
  return {
    type: actionTypes.DELETE_FILE_URL,
  };
};

// ? =========DISPATCH ACTIONS=============

// @desc    Get all cohorts.

export function fetchCohorts(page, term) {
  return (dispatch) => {
    dispatch(getCohortsStart());
    APIClient.get('/cohorts', {
      params: {
        page,
        limit: 10,
      },
    })
      .then((res) => {
        if (res.data.success) {
          dispatch(getCohortsSuccess(res.data));
        } else {
          dispatch(getCohortsFail(res.data.message));
        }
      })
      .catch((error) => {
        if (error.response && error.response.data.message) {
          dispatch(getCohortsFail(error.response.data.message));
        } else {
          dispatch(getCohortsFail('Unable to comunicate with the server.'));
        }
      });
  };
}

// @desc    Get one cohort.

export const fetchCohort = (id) => {
  return (dispatch) => {
    dispatch(getSingleCohortStart());
    APIClient.get(`/cohorts/${id}`)
      .then((res) => {
        if (res.data.success) {
          dispatch(getSingleCohortSuccess(res.data));
        } else {
          dispatch(getSingleCohortFail(res.data.message));
        }
      })
      .catch((err) => {
        if (err.response && err.response.data.message) {
          dispatch(getSingleCohortFail(err.response.data.message));
        } else {
          getSingleCohortFail('Unable to comunicate with the server.');
        }
      });
  };
};

// @desc    Create new cohort.

export function addCohort(data, file) {
  return (dispatch) => {
    if (!file) return dispatch(addCohortFail('Please add file!'));
    dispatch(addCohortStart());
    APIClient.post('/cohorts', data)
      .then((res) => {
        if (res.data.success) {
          dispatch(addCohortSuccess(res.data));
          dispatch(uploadFile(file, res.data.data.cohort._id));
        } else {
          dispatch(addCohortFail(res.data.message));
        }
      })
      .catch((error) => {
        if (error.response && error.response.data.messag) {
          dispatch(addCohortFail(error.response.data.message));
        } else {
          dispatch(addCohortFail('Unable to comunicate with the server.'));
        }
      });
  };
}

// @desc    Edit one cohort.

export const updateCohort = (id, inputData) => {
  return (dispatch) => {
    dispatch(updateCohortStart());
    APIClient.put(`/cohorts/${id}`, inputData)
      .then((res) => {
        if (res.data.success) {
          dispatch(updateCohortSuccess(res.data));
        } else {
          dispatch(updateCohortFail(res.data.message));
        }
      })
      .catch((error) => {
        if (error.response && error.response.data.message) {
          dispatch(updateCohortFail(error.response.data.message));
        } else {
          dispatch(updateCohortFail('Unable to comunicate with the server.'));
        }
      });
  };
};

// @desc    Delete one cohort.

export function deleteCohort(id) {
  return (dispatch) => {
    dispatch(deleteCohortStart());
    APIClient.delete(`/cohorts/${id}`)
      .then((res) => {
        if (res.data && res.data.success) {
          dispatch(deleteCohortSuccess(res.data));
        } else {
          dispatch(deleteCohortFail(res.data.message));
        }
      })
      .catch((error) => {
        if (error.response && error.response.data.message) {
          dispatch(deleteCohortFail(error.response.data.message));
        } else {
          dispatch(deleteCohortFail('Unable to comunicate with the server.!'));
        }
      });
  };
}

export function uploadFile(file, id) {
  return (dispatch) => {
    dispatch(uploadFileStart());
    const fileData = new FormData();
    fileData.append('photo', file);
    const options = {
      onUploadProgress: (progressEvent) => {
        const { loaded, total } = progressEvent;
        const percentage = Math.floor((loaded * 100) / total);

        dispatch(setUploadPercentage(percentage));
      },
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
    };
    APIClient.put(`/cohorts/${id}/photo`, fileData, options)
      .then((res) => {
        if (res.data.success) {
          const { fileURL } = res.data.data;
          dispatch(uploadFileSuccess(fileURL));
          setTimeout(() => {
            setUploadPercentage(0);
          }, 1000);
        } else {
          dispatch(uploadFileFail(res.data.message));
        }
      })
      .catch((error) => {
        if (error.response && error.response.data.message) {
          dispatch(uploadFileFail(error.response.data.message));
        } else {
          dispatch(uploadFileFail('Server is not working!'));
        }
      });
  };
}

export const cleanCohorts = () => {
  return (dispatch) => {
    dispatch({ type: actionTypes.CLEAN_COHORT });
  };
};
