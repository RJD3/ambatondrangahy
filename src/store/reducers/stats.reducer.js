import * as actionTypes from "../types/actionTypes";

const initialState = {
  users: null,
  loading: false,
  error: null,
  user: null,
};

const statsReducer = (state = initialState, action) => {
  switch (action.type) {
    // @desc    Get all reducers.
    case actionTypes.GET_COHORT_USERS_START:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case actionTypes.GET_COHORT_USERS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        users: action.responseData,
      };
    case actionTypes.GET_COHORT_USERS_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    //  ===============================

    //  @desc Set default after update
    case actionTypes.SET_COHORT_USERS_DEFAULT:
      return {
        ...state,
        users: null,
        error: null,
        user: null
      };
    default:
      return state;
  }
};

export default statsReducer;
