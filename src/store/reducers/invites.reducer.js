import * as actionTypes from '../types/actionTypes';

const initialState = {
  invites: null,
  loading: false,
  error: null,
  added: null,
  fullData: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_ALL_INVITES_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.GET_ALL_INVITES_SUCCESS:
      return {
        ...state,
        loading: false,
        invites: action.payload,
        fullData: action.fullData,
      };
    case actionTypes.GET_ALL_INVITES_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    case actionTypes.ADD_INVITE_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.ADD_INVITE_SUCCESS:
      return {
        ...state,
        loading: false,
        added: action.payload,
      };
    case actionTypes.ADD_INVITE_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    case actionTypes.CLEAN_INVITES:
      return {
        ...state,
        invites: null,
        loading: false,
        error: null,
        added: null,
      };
    default:
      return state;
  }
};

export default reducer;
