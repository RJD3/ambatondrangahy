import * as actionTypes from '../types/actionTypes';

const initialState = {
  cohorts: null,
  loading: false,
  error: null,
  cohort: null,
  updatedCohort: null,
  addedCohort: null,
  deletedCohort: null,
  photo: null,
  fileURL: null,
  percentage: null,
};

const cohortsReducer = (state = initialState, action) => {
  switch (action.type) {
    // @desc    Get all reducers.
    case actionTypes.GET_COHORTS_START:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case actionTypes.GET_COHORTS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        cohorts: action.responseData,
      };
    case actionTypes.GET_COHORTS_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    //  ===============================

    //@desc Create new cohort
    case actionTypes.ADD_COHORT_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.ADD_COHORT_SUCCESS:
      return {
        ...state,
        loading: false,
        addedCohort: action.added,
        error: null,
      };
    case actionTypes.ADD_COHORT_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    // ===============================

    //  @desc   Update one cohort
    case actionTypes.EDIT_COHORT_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.EDIT_COHORT_SUCCESS:
      return {
        ...state,
        loading: false,
        updatedCohort: action.updated,
        error: null,
      };
    case actionTypes.EDIT_COHORT_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error,
      };

    //  @desc   Delete one cohort
    case actionTypes.DELETE_COHORT_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.DELETE_COHORT_SUCCESS:
      return {
        ...state,
        loading: false,
        deletedCohort: action.deleted,
        error: null,
      };
    case actionTypes.DELETE_COHORT_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error,
      };

    //  @desc   Get single cohort.
    case actionTypes.GET_COHORT_START:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case actionTypes.GET_COHORT_SUCCESS:
      return {
        ...state,
        loading: false,
        cohort: action.fetched,
      };
    case actionTypes.GET_COHORT_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    //  =================================

    case actionTypes.COHORT_START:
      return {
        ...state,
        loading: true,
        reported: false,
      };
    case actionTypes.COHORT_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error,
        success: false,
        reported: false,
      };
    case actionTypes.COHORT_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: null,
        reported: action.reported,
      };
    }

    // @desc   Upload one file
    case actionTypes.UPLOAD_FILE_START:
      return {
        ...state,
        loading: true,
        fileURL: null,
        percentage: 0,
      };
    case actionTypes.UPLOAD_FILE_SUCCESS:
      return {
        ...state,
        loading: false,
        fileURL: action.fileURL,
      };
    case actionTypes.UPLOAD_FILE_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    case actionTypes.SET_UPLOAD_PERCENTAGE:
      return {
        ...state,
        percentage: action.percentage,
      };
    //======================================

    //  @desc Set default after update
    case actionTypes.CLEAN_COHORT:
      return {
        ...state,
        cohorts: null,
        error: null,
        cohort: null,
        updatedCohort: null,
        addedCohort: null,
        deletedCohort: null,
        percentage: null,
        photo: null,
      };
    case actionTypes.DELETE_FILE_URL:
      return {
        ...state,
        fileURL: null,
      };
    default:
      return state;
  }
};

export default cohortsReducer;
