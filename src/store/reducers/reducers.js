import { combineReducers } from 'redux';

import settingsReducer from './settings.reducer.js';
import themesReducer from './themes.reducers.js';
import cohortsReducer from './cohorts.recuder';
import loginData from './login.reducer';
import tokenReducers from './token.reducer.js';
import statsReducer from './stats.reducer';
import reportsReducer from './reports.reducer';
import invitesReducer from './invites.reducer';
import usersReducer from './users.reducer.js';
import gangsReducer from './gangs.reducer';
import badgesReducer from './badges.reducer';

export default combineReducers({
  settings: settingsReducer,
  theme: themesReducer,
  token: tokenReducers,
  cohorts: cohortsReducer,
  stats: statsReducer,
  loginData,
  reports: reportsReducer,
  invites: invitesReducer,
  users: usersReducer,
  gangs: gangsReducer,
  badges: badgesReducer,
});
