import {
  GET_ALL_BADGES_START,
  GET_ALL_BADGES_SUCCESS,
  GET_ALL_BADGES_FAILED,
  GET_ALL_BADGES_RESET,
  GET_BADGE_USERS_START,
  GET_BADGE_USERS_SUCCESS,
  GET_BADGE_USERS_FAILED,
  GET_BADGE_USERS_RESET,
  GET_BADGE_CANDIDATES_START,
  GET_BADGE_CANDIDATES_SUCCESS,
  GET_BADGE_CANDIDATES_FAILED,
  GET_BADGE_CANDIDATES_RESET,
  GET_ONE_BADGE_START,
  GET_ONE_BADGE_SUCCESS,
  GET_ONE_BADGE_FAILED,
  GET_ONE_BADGE_RESET,
  CREATE_BADGE_START,
  CREATE_BADGE_SUCCESS,
  CREATE_BADGE_FAILED,
  CREATE_BADGE_RESET,
  ASSIGN_BADGE_START,
  ASSIGN_BADGE_SUCCESS,
  ASSIGN_BADGE_FAILED,
  ASSIGN_BADGE_RESET,
  REMOVE_BADGE_START,
  REMOVE_BADGE_SUCCESS,
  REMOVE_BADGE_FAILED,
  REMOVE_BADGE_RESET,
  UPDATE_ONE_BADGE_START,
  UPDATE_ONE_BADGE_SUCCESS,
  UPDATE_ONE_BADGE_FAILED,
  UPDATE_ONE_BADGE_RESET,
  DELETE_ONE_BADGE_START,
  DELETE_ONE_BADGE_SUCCESS,
  DELETE_ONE_BADGE_FAILED,
  DELETE_ONE_BADGE_RESET,
} from '../types/actionTypes';

const initialLoadingState = {
  loading: false,
  success: false,
  data: null,
  error: false,
  errorMessage: null,
};

const initialState = {
  getAll: initialLoadingState,
  getBadgeUsers: initialLoadingState,
  getBadgeCandidates: initialLoadingState,
  getOne: initialLoadingState,
  create: initialLoadingState,
  assignBadge: initialLoadingState,
  removeBadge: initialLoadingState,
  updateOne: initialLoadingState,
  deleteOne: initialLoadingState,
};

const badgesReducer = (state = initialState, action) => {
  switch (action.type) {
    // Get All Badges.
    case GET_ALL_BADGES_START:
      return { ...state, getAll: { ...action.payload } };
    case GET_ALL_BADGES_SUCCESS:
      return { ...state, getAll: { ...action.payload } };
    case GET_ALL_BADGES_FAILED:
      return { ...state, getAll: { ...action.payload } };
    case GET_ALL_BADGES_RESET:
      return { ...state, getAll: { ...initialLoadingState } };
    // Get Badge Users.
    case GET_BADGE_USERS_START:
      return { ...state, getBadgeUsers: { ...action.payload } };
    case GET_BADGE_USERS_SUCCESS:
      return { ...state, getBadgeUsers: { ...action.payload } };
    case GET_BADGE_USERS_FAILED:
      return { ...state, getBadgeUsers: { ...action.payload } };
    case GET_BADGE_USERS_RESET:
      return { ...state, getBadgeUsers: { ...initialLoadingState } };
    // Get Badge Candidates.
    case GET_BADGE_CANDIDATES_START:
      return { ...state, getBadgeCandidates: { ...action.payload } };
    case GET_BADGE_CANDIDATES_SUCCESS:
      return { ...state, getBadgeCandidates: { ...action.payload } };
    case GET_BADGE_CANDIDATES_FAILED:
      return { ...state, getBadgeCandidates: { ...action.payload } };
    case GET_BADGE_CANDIDATES_RESET:
      return { ...state, getBadgeCandidates: { ...initialLoadingState } };
    // Get One Badge.
    case GET_ONE_BADGE_START:
      return { ...state, getOne: { ...action.payload } };
    case GET_ONE_BADGE_SUCCESS:
      return { ...state, getOne: { ...action.payload } };
    case GET_ONE_BADGE_FAILED:
      return { ...state, getOne: { ...action.payload } };
    case GET_ONE_BADGE_RESET:
      return { ...state, getOne: { ...initialLoadingState } };
    // Create Badge.
    case CREATE_BADGE_START:
      return { ...state, create: { ...action.payload } };
    case CREATE_BADGE_SUCCESS:
      return { ...state, create: { ...action.payload } };
    case CREATE_BADGE_FAILED:
      return { ...state, create: { ...action.payload } };
    case CREATE_BADGE_RESET:
      return { ...state, create: { ...initialLoadingState } };
    // Assign Badge To User.
    case ASSIGN_BADGE_START:
      return { ...state, assignBadge: { ...action.payload } };
    case ASSIGN_BADGE_SUCCESS:
      return { ...state, assignBadge: { ...action.payload } };
    case ASSIGN_BADGE_FAILED:
      return { ...state, assignBadge: { ...action.payload } };
    case ASSIGN_BADGE_RESET:
      return { ...state, assignBadge: { ...initialLoadingState } };
    // Remove Badge From User.
    case REMOVE_BADGE_START:
      return { ...state, removeBadge: { ...action.payload } };
    case REMOVE_BADGE_SUCCESS:
      return { ...state, removeBadge: { ...action.payload } };
    case REMOVE_BADGE_FAILED:
      return { ...state, removeBadge: { ...action.payload } };
    case REMOVE_BADGE_RESET:
      return { ...state, removeBadge: { ...initialLoadingState } };
    // Update One Badge.
    case UPDATE_ONE_BADGE_START:
      return { ...state, updateOne: { ...action.payload } };
    case UPDATE_ONE_BADGE_SUCCESS:
      return { ...state, updateOne: { ...action.payload } };
    case UPDATE_ONE_BADGE_FAILED:
      return { ...state, updateOne: { ...action.payload } };
    case UPDATE_ONE_BADGE_RESET:
      return { ...state, updateOne: { ...initialLoadingState } };
    // Delete One Badge.
    case DELETE_ONE_BADGE_START:
      return { ...state, deleteOne: { ...action.payload } };
    case DELETE_ONE_BADGE_SUCCESS:
      return { ...state, deleteOne: { ...action.payload } };
    case DELETE_ONE_BADGE_FAILED:
      return { ...state, deleteOne: { ...action.payload } };
    case DELETE_ONE_BADGE_RESET:
      return { ...state, deleteOne: { ...initialLoadingState } };
    default:
      return state;
  }
};

export default badgesReducer;
