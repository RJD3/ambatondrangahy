import {
  GET_ALL_GANGS_START,
  GET_ALL_GANGS_SUCCESS,
  GET_ALL_GANGS_FAILED,
  GET_ALL_GANGS_RESET,
  GET_ONE_GANG_START,
  GET_ONE_GANG_SUCCESS,
  GET_ONE_GANG_FAILED,
  GET_ONE_GANG_RESET,
  CREATE_GANG_START,
  CREATE_GANG_SUCCESS,
  CREATE_GANG_FAILED,
  CREATE_GANG_RESET,
  UPDATE_ONE_GANG_START,
  UPDATE_ONE_GANG_SUCCESS,
  UPDATE_ONE_GANG_FAILED,
  UPDATE_ONE_GANG_RESET,
  DELETE_ONE_GANG_START,
  DELETE_ONE_GANG_SUCCESS,
  DELETE_ONE_GANG_FAILED,
  DELETE_ONE_GANG_RESET,
} from '../types/actionTypes';

const initialLoadingState = {
  loading: false,
  success: false,
  data: null,
  error: false,
  errorMessage: null,
};

const initialState = {
  getAll: initialLoadingState,
  getOne: initialLoadingState,
  create: initialLoadingState,
  updateOne: initialLoadingState,
  deleteOne: initialLoadingState,
};

const gangsReducer = (state = initialState, action) => {
  switch (action.type) {
    // Get All Gangs.
    case GET_ALL_GANGS_START:
      return { ...state, getAll: { ...action.payload } };
    case GET_ALL_GANGS_SUCCESS:
      return { ...state, getAll: { ...action.payload } };
    case GET_ALL_GANGS_FAILED:
      return { ...state, getAll: { ...action.payload } };
    case GET_ALL_GANGS_RESET:
      return { ...state, getAll: { ...initialLoadingState } };
    // Get One Gang.
    case GET_ONE_GANG_START:
      return { ...state, getOne: { ...action.payload } };
    case GET_ONE_GANG_SUCCESS:
      return { ...state, getOne: { ...action.payload } };
    case GET_ONE_GANG_FAILED:
      return { ...state, getOne: { ...action.payload } };
    case GET_ONE_GANG_RESET:
      return { ...state, getOne: { ...initialLoadingState } };
    // Create Gang.
    case CREATE_GANG_START:
      return { ...state, create: { ...action.payload } };
    case CREATE_GANG_SUCCESS:
      return { ...state, create: { ...action.payload } };
    case CREATE_GANG_FAILED:
      return { ...state, create: { ...action.payload } };
    case CREATE_GANG_RESET:
      return { ...state, create: { ...initialLoadingState } };
    // Update One Gang.
    case UPDATE_ONE_GANG_START:
      return { ...state, updateOne: { ...action.payload } };
    case UPDATE_ONE_GANG_SUCCESS:
      return { ...state, updateOne: { ...action.payload } };
    case UPDATE_ONE_GANG_FAILED:
      return { ...state, updateOne: { ...action.payload } };
    case UPDATE_ONE_GANG_RESET:
      return { ...state, updateOne: { ...initialLoadingState } };
    // Delete One Gang.
    case DELETE_ONE_GANG_START:
      return { ...state, deleteOne: { ...action.payload } };
    case DELETE_ONE_GANG_SUCCESS:
      return { ...state, deleteOne: { ...action.payload } };
    case DELETE_ONE_GANG_FAILED:
      return { ...state, deleteOne: { ...action.payload } };
    case DELETE_ONE_GANG_RESET:
      return { ...state, deleteOne: { ...initialLoadingState } };
    default:
      return state;
  }
};

export default gangsReducer;
