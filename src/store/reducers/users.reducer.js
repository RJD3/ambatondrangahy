import * as actionTypes from "../types/actionTypes";

const initialState = {
    users:null,
    loading: false,
    error: null,
};

const usersReducer = (state = initialState, action) => {
    
    switch (action.type) {
        case actionTypes.GET_ALL_USERS_START:
            return {
                ...state,
                loading: true,
                error: null,
              };
        case actionTypes.GET_ALL_USERS_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                users: action.payload,
              };
        case actionTypes.GET_ALL_USERS_FAIL:
            return {
                ...state,
                loading:false,
                error:action.error,
                users:null
            }
        case actionTypes.CLEAN_USERS:
            return initialState;
        default:
            return state;
    }

};

export default usersReducer;
