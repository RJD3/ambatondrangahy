/* eslint-disable vars-on-top */
import { TOKEN, DELETE_TOKEN } from '../types/actionTypes';

const initialToken = null;

const tokenReducers = (state = initialToken, action) => {
  switch (action.type) {
    case TOKEN:
      // eslint-disable-next-line no-var
      var token = action.value;
      return token;
    case DELETE_TOKEN:
      return null;
    default:
      return state;
  }
};

export default tokenReducers;
