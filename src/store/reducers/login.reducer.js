import * as actionTypes from "../types/actionTypes";

const initialData = {
  token: null,
  error: null,
  loading: false,
  loginSuccess:null
};

const INITIAL_STATE = {
    error: '',
};

const loginData = (state = initialData, action) => {
    switch (action.type){
        // @desc Create new one
        case actionTypes.LOGIN_START:
            return{
                ...state,
                loading: true,
            };
        case actionTypes.LOGIN_SUCCESS:
            return {
                ...state,
                loginSuccess:action.payload
            }
        case actionTypes.CLEAN_LOGIN_SUCCESS:
            return {
                ...state,
                loginSuccess:null
            }
        
        case actionTypes.CONFIRM_SUCCESS:
            return {
                ...state,
                token: action.token,
                loading: false,
                error: null
            }
        
        case actionTypes.LOGIN_FAIL:
            return {
                ...state,
                loading: false,
                error: action.error,
            }
        //===============================
        case actionTypes.CLEAR:
            return INITIAL_STATE;
        case actionTypes.LOGOUT:
            return initialData;
        case actionTypes.LOG_OUT:
            return{
                ...state,
                loading: false,
                error:null,
                token:null,
            }
        case actionTypes.CLEAN_LOGIN_ERROR:
            return {
                ...state,
                error: null,
            };
        default:
            return state;
    }
}
export default loginData;