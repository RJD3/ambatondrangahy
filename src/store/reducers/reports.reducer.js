import * as actionTypes from '../types/actionTypes';

const initialState = {
  reports: null,
  report: null,
  loading: false,
  error: null,
  flaged: false,
  loadingFlag: false,
  fullData: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_ALL_REPORTS_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.GET_ALL_REPORTS_SUCCESS:
      return {
        ...state,
        loading: false,
        reports: action.payload,
        fullData: action.fullData,
      };
    case actionTypes.GET_ALL_REPORTS_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    case actionTypes.GET_ONE_REPORT_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.GET_ONE_REPORT_SUCCESS:
      return {
        ...state,
        loading: false,
        report: action.payload,
      };
    case actionTypes.GET_ONE_REPORT_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    case actionTypes.FLAG_REPORT_START:
      return {
        ...state,
        loadingFlag: true,
      };
    case actionTypes.FLAG_REPORT_SUCCESS:
      return {
        ...state,
        loadingFlag: false,
        flaged: true,
      };
    case actionTypes.FLAG_REPORT_FAIL:
      return {
        ...state,
        loadingFlag: false,
        error: action.error,
      };
    case actionTypes.CLEAN_REPORTS:
      return {
        ...state,
        reports: null,
        report: null,
        loading: false,
        error: null,
        flaged: false,
        loadingFlag: false,
      };
    case actionTypes.CLEAN_FLAG:
      return {
        ...state,
        flaged: false,
      };
    default:
      return state;
  }
};

export default reducer;
