import React, { Suspense, lazy } from 'react';
import { withRouter, Switch, Route, Redirect } from 'react-router-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import jwt from 'jsonwebtoken';
import { connect } from 'react-redux';

/* loader component for Suspense*/
import PageLoader from './components/Common/PageLoader';

import Base from './components/Layout/Base';
import BasePage from './components/Layout/BasePage';
// import BaseHorizontal from './components/Layout/BaseHorizontal';

/* Used to render a lazy component with react-router */
const waitFor = (Tag) => (props) => <Tag {...props} />;

const Login = lazy(() => import('./components/Login/Login'));
const Confirm = lazy(() => import('./components/Confirm/Confirm'));
const AdminView = lazy(() => import('./components/Cohorts/Cohorts'));
const SubMenu = lazy(() => import('./components/SubMenu/SubMenu'));
const EditCohorts = lazy(() => import('./components/Cohorts/EditCohorts'));
const Stats = lazy(() => import('./components/Cohorts/Stats'));
const Reports = lazy(() => import('./components/Reports/Reports'));
const ReportDetailView = lazy(() => import('./components/Reports/ReportDetailView'));
const Invites = lazy(() => import('./components/Invites/Invites'));
const AddInvite = lazy(() => import('./components/Invites/AddInvite'));
const Users = lazy(() => import('./components/Users/Users'));
const ListGang = lazy(() => import('./components/Gangs/List'));
const CreateGang = lazy(() => import('./components/Gangs/Create'));
const EditGang = lazy(() => import('./components/Gangs/Edit'));
const ListBadge = lazy(() => import('./components/Badges/List'));
const CreateBadge = lazy(() => import('./components/Badges/Create'));
const EditBadge = lazy(() => import('./components/Badges/Edit'));
// List of routes that uses the page layout
// listed here to Switch between layouts
// depending on the current pathname
const listofPages = [
  /* See full project for reference */
  '/login',
  '/confirm',
];

const Routes = ({ location, token }) => {
  const currentKey = location.pathname.split('/')[1] || '/';
  const timeout = { enter: 500, exit: 500 };
  // Animations supported
  //      'rag-fadeIn'
  //      'rag-fadeInRight'
  //      'rag-fadeInLeft'

  const decodedToken = jwt.decode(token);

  const animationName = 'rag-fadeIn';

  if (listofPages.indexOf(location.pathname) > -1) {
    if (token == null || typeof token == 'undefined') {
      return (
        // Page Layout component wrapper
        <BasePage>
          <Suspense fallback={<PageLoader />}>
            <Switch location={location}>
              <Route path="/login" component={waitFor(Login)} />
              <Route path="/confirm" component={waitFor(Confirm)} />

              {/* See full project for reference */}
            </Switch>
          </Suspense>
        </BasePage>
      );
    } else {
      return (
        // Page Layout component wrapper
        <BasePage>
          <Suspense fallback={<PageLoader />}>
            <Switch location={location}>
              <Route path="/adminview" component={waitFor(AdminView)} />
              <Redirect to="/adminview" />
              {/* See full project for reference */}
            </Switch>
          </Suspense>
        </BasePage>
      );
    }
  } else {
    if (token == null || typeof token == 'undefined') {
      return (
        <Base>
          <TransitionGroup>
            <CSSTransition key={currentKey} timeout={timeout} classNames={animationName} exit={false}>
              <div>
                <Suspense fallback={<PageLoader />}>
                  <Switch location={location}>
                    <Route path="/confirm" component={waitFor(Confirm)} />
                    <Route path="/login" component={waitFor(Login)} />
                    <Redirect to="/login" />
                  </Switch>
                </Suspense>
              </div>
            </CSSTransition>
          </TransitionGroup>
        </Base>
      );
      // Layout component wrapper
      // Use <BaseHorizontal> to change layout
    } else if (decodedToken.role === 5) {
      return (
        // Layout component wrapper
        // Use <BaseHorizontal> to change layout
        <Base>
          <TransitionGroup>
            <CSSTransition key={currentKey} timeout={timeout} classNames={animationName} exit={false}>
              <div>
                <Suspense fallback={<PageLoader />}>
                  <Switch location={location}>
                    <Route path="/adminview" component={waitFor(AdminView)} />
                    <Route path="/submenu" component={waitFor(SubMenu)} />
                    <Route path="/stats" component={waitFor(Stats)} />
                    <Route exact path="/editCohorts" component={waitFor(EditCohorts)} />
                    <Route exact path="/reports" component={waitFor(Reports)} />
                    <Route path="/reports/:id" component={waitFor(ReportDetailView)} />
                    <Route exact path="/invites" component={waitFor(Invites)} />
                    <Route exact path="/addinvite" component={waitFor(AddInvite)} />
                    <Route path="/editCohorts/:id" component={waitFor(EditCohorts)} />
                    <Route path="/:id/stats" component={waitFor(Stats)} />
                    <Route path="/users" component={waitFor(Users)} />
                    <Route exact path="/gangs" component={waitFor(ListGang)} />
                    <Route exact path="/gangs/create" component={waitFor(CreateGang)} />
                    <Route exact path="/gangs/edit/:id" component={waitFor(EditGang)} />
                    <Route exact path="/badges" component={waitFor(ListBadge)} />
                    <Route exact path="/badges/create" component={waitFor(CreateBadge)} />
                    <Route exact path="/badges/edit/:id" component={waitFor(EditBadge)} />
                    <Redirect to="/adminview" />
                  </Switch>
                </Suspense>
              </div>
            </CSSTransition>
          </TransitionGroup>
        </Base>
      );
    } else {
      return (
        // Layout component wrapper
        // Use <BaseHorizontal> to change layout
        <Base>
          <TransitionGroup>
            <CSSTransition key={currentKey} timeout={timeout} classNames={animationName} exit={false}>
              <div>
                <Suspense fallback={<PageLoader />}>
                  <Switch location={location}>
                    <Route exact path="/editCohorts" component={waitFor(EditCohorts)} />
                    <Route path="/editCohorts/:id" component={waitFor(EditCohorts)} />
                    <Route exact path="/stats" component={waitFor(Stats)} />
                    <Route path="/:id/stats" component={waitFor(Stats)} />
                    <Redirect to="/adminview" />
                  </Switch>
                </Suspense>
              </div>
            </CSSTransition>
          </TransitionGroup>
        </Base>
      );
    }
  }
};

function mapStateToProps(state) {
  return {
    token: state.loginData.token !== '' ? state.loginData.token : null,
  };
}

export default withRouter(connect(mapStateToProps)(Routes));
