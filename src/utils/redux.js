export const dispatchLoading = (dispatch, action) => {
  dispatch(action({ loading: true, success: false, data: null, error: false, errorMessage: null }));
};
export const dispatchSuccess = (dispatch, action, data, propName) => {
  dispatch(action({ loading: false, success: true, data: { [propName]: data }, error: false, errorMessage: null }));
};
export const dispatchError = (dispatch, action, errorMessage) => {
  dispatch(action({ loading: false, success: false, data: null, error: true, errorMessage: errorMessage }));
};
export const getAxiosError = (error) => {
  return error.response && error.response.data && error.response.data.error
    ? error.response.data.error
    : 'Something went wrong!';
};
