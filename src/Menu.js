const Menu = [
  {
    name: 'Cohorts',
    path: '/adminview',
    icon: 'icon-grid',
  },
  {
    name: 'Reports',
    path: '/reports',
    icon: 'icon-ban',
  },
  {
    name: 'Invites',
    path: '/invites',
    icon: 'icon-people',
  },
  {
    name: 'Users',
    path: '/users',
    icon: 'icon-people',
  },
  {
    name: 'Gangs',
    path: '/gangs',
    icon: 'icon-people',
  },
  {
    name: 'Badges',
    path: '/badges',
    icon: 'icon-badge',
  },
];

export default Menu;
